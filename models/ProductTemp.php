<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "product_temp".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $code
 * @property string $name
 * @property integer $price
 * @property string $provider
 * @property integer $provider_price
 * @property integer $count
 * @property integer $total
 *
 * @property User $user
 */
class ProductTemp extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_temp';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'code', 'name', 'price', 'provider', 'provider_price', 'count', 'total'], 'required'],
            [['user_id', 'price', 'provider_price', 'count', 'total'], 'integer'],
            [['code', 'name', 'provider'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'code' => Yii::t('app', 'Code'),
            'name' => Yii::t('app', 'Name'),
            'price' => Yii::t('app', 'Price'),
            'provider' => Yii::t('app', 'Provider'),
            'provider_price' => Yii::t('app', 'Provider Price'),
            'count' => Yii::t('app', 'Count'),
            'total' => Yii::t('app', 'Total'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public static function migrateToOrder($order_id)
    {
        $tempOrders = self::find()->asArray()->where(['user_id' => Yii::$app->user->getId()])->all();
        $i = 0;
        foreach ($tempOrders as $tempOrder)
        {
            unset($tempOrders[$i]['id']);
            unset($tempOrders[$i]['user_id']);
            $tempOrders[$i]['order_id'] = $order_id;
            $productModel = new Product();
            $productModel->attributes = $tempOrders[$i];
            $productModel->save();
            $i++;
        }
        Yii::$app
            ->db
            ->createCommand()
            ->delete(ProductTemp::tableName(), 'user_id='.Yii::$app->user->getId())
            ->execute();
    }
}
