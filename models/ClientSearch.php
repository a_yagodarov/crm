<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Client;

/**
 * ClientSearch represents the model behind the search form about `app\models\Client`.
 */
class ClientSearch extends Client
{
    public $order_send_num;
    public $active_tasks;
    public $task_date_end;
	public $tracking_number;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'created_at', 'updated_at', 'active_tasks'], 'integer'],
            [['name', 'email', 'phone_work', 'order_send_num', 'active_tasks', 'task_date_end', 'tracking_number'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }



    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Client Name'),
            'order_send_num' => Yii::t('app', 'Order Send Num'),
            'tracking_number' => Yii::t('app', 'Tracking Number'),
            'active_tasks' => Yii::t('app', 'Active Tasks'),
            'task_date_end' => Yii::t('app', 'Task Date End'),
            'email' => Yii::t('app', 'Email')
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Client::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (Yii::$app->user->identity->role_id == '2')
            $user_id = Yii::$app->user->identity->getId();
        else
            $user_id = '';

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $query->andFilterWhere([
            'user_id' => $user_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->joinWith(Task::tableName());

        if ($this->active_tasks == '1')
        {
            $query->andFilterWhere(
                ['task.status' => $this->active_tasks]
            );
        }
	    $query->joinWith(Order::tableName());
	    $query->andFilterWhere(['like', 'order.tracking_number', $this->tracking_number]);
	    $query->andFilterWhere(['like', 'order.order_send_num', $this->order_send_num]);
        if ($this->task_date_end)
            $this->task_date_end = date('Y-m-d H:i:s', strtotime($this->task_date_end));
        $query->andFilterWhere(
            ['task.date_end' => $this->task_date_end]
        );

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere([
                'or',
                ['like', 'email', $this->email],
                ['like', 'email_2', $this->email]
            ])
            ->andFilterWhere([
                'or',
                ['like', 'phone_work', $this->phone_work],
                ['like', 'phone_mob', $this->phone_work],
                ['like', 'phone_home', $this->phone_work],
            ]);

        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
    }
}
