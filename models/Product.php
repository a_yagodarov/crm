<?php

namespace app\models;

use Yii;
use yii\db\Query;
/**
 * This is the model class for table "product".
 *
 * @property integer $id
 * @property integer $order_id
 * @property string $code
 * @property string $name
 * @property integer $price
 * @property string $provider
 * @property integer $provider_price
 * @property integer $count
 * @property integer $total
 * @property integer $weight
 * @property integer $total_weight
 *
 * @property Order $order
 */
class Product extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['price', 'provider_price', 'count', 'total', 'weight', 'total_weight'], 'integer'],
            [['code', 'name', 'price', 'provider_price', 'count'], 'required'],
            [['code', 'name', 'provider'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'order_id' => Yii::t('app', 'Order ID'),
            'code' => Yii::t('app', 'Code'),
            'name' => Yii::t('app', 'Name'),
            'price' => Yii::t('app', 'Price'),
            'provider' => Yii::t('app', 'Provider'),
            'provider_price' => Yii::t('app', 'Provider Price'),
            'count' => Yii::t('app', 'Count'),
            'total' => Yii::t('app', 'Total'),
            'weight' => Yii::t('app', 'Weight'),
            'total_weight' => Yii::t('app', 'Weight')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */

   public static function getProductArrayForForm()
   {
       $schema = Product::getTableSchema();
       $columns = (array)$schema->columns;
       $resultArr = [];
       foreach($columns as $column)
       {
           $column = (array)$column;
           if ($column['name'] != 'order_id')
           {
               $resultArr[$column ['name']] = [
                   'name' => $column['name'],
                   'defaultValue' => $column['defaultValue'],
                   'type' => $column['type'],
                   'value' => '',
                   'allowNull' => $column['allowNull']
               ];
           }
       }
       if ($resultArr) return $resultArr;
   }
   public static function getOrderProductsById($order_id)
   {
       $productForm = self::getProductArrayForForm();
       $products = (new Query())
           ->select('id, code, name, price, weight, count, provider, provider_price, total_weight, total')
           ->from('product')
           ->andWhere(['order_id' => $order_id])
           ->all();
       $resultArr = [];
       foreach ($products as $product)
       {
           $newProduct = $productForm;
           foreach($newProduct as $column)
           {
               $prop = $column['name'];
               $value = $product[$prop];
               $newProduct[$column['name']]['value'] = $value;
           }
           $resultArr[] = $newProduct;
       }
       return $resultArr;
   }
   public static function updateProducts($id)
   {
//       $deleteQuery = "delete product
//               from product
//               inner join order_products ON order_products.product_id = product.id
//               where order_products.order_id =".$id;
//       $delete = Yii::$app->db->createCommand($deleteQuery)
//           ->query();
   }
   public static function insertProducts($products, $order_id)
   {
       $query = (Yii::$app->db->createCommand('delete product from product where product.order_id=' . $order_id)
           ->execute());
       if ($products != null)
           foreach ($products as $product) {
                {
                    $row = new self;
                    $row->attributes = $product;
                    $row->order_id = $order_id;
                    $row->total = $row->count * $row->price;
                    $row->total_weight = $row->count * $row->weight;
                    $errors = $row->getErrors();
                    if ($row->validate())
                       $result = $row->save();
               }
           }
       Order::updateOrderTotalSum($order_id);
   }



    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }
}
