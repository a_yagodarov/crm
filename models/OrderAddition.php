<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "order_addition".
 *
 * @property integer $id
 * @property integer $order_id
 * @property string $name
 * @property string $value
 *
 * @property Order $order
 */
class OrderAddition extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order_addition';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'order_id'], 'integer'],
            [['name', 'value'], 'string', 'max' => 128]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'name' => 'Name',
            'value' => 'Value',
        ];
    }

    /*
     *
     */

    public static function getOrderAdditionFormColumns()
    {
        $schema = self::getTableSchema();
        $columns = $schema['columns'];
        foreach ($columns as $column)
        {

        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }
}
