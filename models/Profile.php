<?php
namespace app\models;

use dektrium\user\models\Profile as BaseProfile;
use Yii;
/**
 * This is the model class for table "Profile".
* @property string $days
 *
 */
class Profile extends BaseProfile
{
	public function rules()
	{
		return [
			'nameLength' => ['name', 'string', 'max' => 255],
			[['public_email'], 'email'],
			[['phone', 'from', 'to', 'active'], 'number'],
			[['from', 'to'], 'number', 'min' => 0, 'max' => 24],
//			[['from'], 'required',
//				'when' => function ($model) {
//					return true;
//				},
//				'whenClient' => "function (attribute, value) {
//				console.log(123);
//					return true;
//    			}"
//			],
			[['public_email', 'days', 'phone', 'salary', 'active', 'apply'], 'safe']
		];
	}

	/** @inheritdoc */
	public function attributeLabels()
	{
		return [
			'name'           => Yii::t('user', 'ФИО'),
			'public_email'           => Yii::t('app', 'Email'),
			'phone'          => Yii::t('user', 'Телефон'),
			'salary'          => Yii::t('app', 'Salary'),
			'from'          => Yii::t('app', 'From'),
			'to'          => Yii::t('app', 'To'),
			'active'          => Yii::t('app', 'Active'),
			'days' => Yii::t('app', 'Days Active'),
			'apply' => Yii::t('app', 'Apply')
		];
	}

	public function beforeSave($insert)
	{
		if (parent::beforeSave($insert)) {
			return true;
		}

		return false;
	}

	public static function haveAccess()
	{
		if (Yii::$app->user->identity->role_id == User::ROLE_ADMIN)
			return true;
		$profile = Profile::findOne(['user_id' => Yii::$app->user->identity->getId()]);
		if ($profile->apply == 0 || $profile->apply == NULL)
			return true;
		$days = json_decode($profile->days);
		$thisDay = date('N');
		if (!in_array($thisDay, $days))
			return false;
		$from = $profile->from;
		$to = $profile->to;
		$hour = intval(date('H'));
		if (!($from < $hour && $to > $hour))
			return false;
		return true;
	}
}
