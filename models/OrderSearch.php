<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Order;

/**
 * OrderSearch represents the model behind the search form about `app\models\Order`.
 */
class OrderSearch extends Model
{
    /**
     * @inheritdoc
     */
    public $client_id;
    public $date_create;
    public $date_update;
    public $date_book;
    public $date_end;
    public $date_send;
    public $delivery_method_id;
    public $payment_method_id;
    public $order_status_id;
    public $total;
    public $order_send_num;
    public $tracking_number;
    public $have_active_tasks;

    public function rules()
    {
        return [
            [
                [
                    'date_create', 'date_update', 'date_book', 'date_end', 'date_send', 'delivery_method_id',
                    'payment_method_id', 'order_status_id', 'total', 'order_send_num', 'tracking_number', 'total'
                ],
                    'integer'
            ],
            [
                [
                    'client_id'
                ],
                    'string'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'client_id' => 'Client ID',
            //'name' => Yii::t('app', 'Name Manager'),
            'date_create' => Yii::t('app', 'Date Create'),
            'date_update' => Yii::t('app', 'Date Update'),
            'date_book' => Yii::t('app', 'Date Booking'),
            'date_end' => Yii::t('app', 'Date End'),
            'date_send' => Yii::t('app', 'Date Send'),
            'delivery_method_id' => Yii::t('app', 'Delivery Method'),
            'payment_method_id' => Yii::t('app', 'Payment Method'),
            'order_status_id' => Yii::t('app', 'Order Status'),
            'order_send_num' => Yii::t('app', 'Order Send Num'),
            'tracking_number' => Yii::t('app', 'Tracking Number'),
            'total' => Yii::t('app', 'Total'),
            'comment' => Yii::t('app', 'Comment')
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Order::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 50
            ]
        ]);

        $this->load($params);

        if (Yii::$app->user->identity->role_id == 2)
            $query->andFilterWhere(['user_id' => Yii::$app->user->identity->getId()]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $query->joinWith('client');
        $query->andFilterWhere([
            'like',
            'client.name',
            $this->client_id
        ]);
        $query->andFilterWhere([
            'like',
            'tracking_number',
            $this->tracking_number
        ]);
        $query->andFilterWhere([
            'date_create' => $this->date_create,
            'date_update' => $this->date_update,
            'date_book' => $this->date_book,
            'date_end' => $this->date_end,
            'date_send' => $this->date_send,
            'delivery_method_id' => $this->delivery_method_id,
            'payment_method_id' => $this->payment_method_id,
            'order_status_id' => $this->order_status_id,
            'order.order_send_num' => $this->order_send_num,
            'total' => $this->total,
        ]);

        return $dataProvider;
    }
}
