<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "address".
 *
 * @property integer $id
 * @property integer $client_id
 * @property string $town
 * @property string $street
 * @property integer $house
 * @property integer $flat
 * @property integer $porch
 * @property string $organisation_name
 * @property string $comment
 *
 * @property Client $client
 */
class Address extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'address';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['town', 'street', 'house'], 'required'],
            [['client_id', 'flat', 'porch'], 'integer'],
            [['house'], 'string', 'max' => 75],
            [['town', 'street'], 'string', 'max' => 75],
            [['organisation_name', 'comment'], 'string', 'max' => 128]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'client_id' => 'Client ID',
            'town' => Yii::t('app', 'Town'),
            'street' => Yii::t('app', 'Street'),
            'house' => Yii::t('app', 'House'),
            'flat' => Yii::t('app', 'Flat'),
            'porch' => Yii::t('app', 'Porch'),
            'organisation_name' => Yii::t('app', 'Organisation Name'),
            'comment' => Yii::t('app', 'Comment'),
        ];
    }

    public static function filterSearch($params)
    {
        $id = $params['client_id'];
        //$query = Address::find()->where(['client_id' => '$id']);
        return new ActiveDataProvider([
            'query' => Address::find()->where(['client_id' => $id]),
        ]);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['id' => 'client_id']);
    }
}
