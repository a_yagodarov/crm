<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Query;

/**
 * This is the model class for table "order".
 *
 * @property integer $id
 * @property integer $client_id
 * @property integer $date_create
 * @property integer $date_update
 * @property integer $date_send
 * @property integer $date_end
 * @property integer $date_book
 * @property integer $delivery_method_id
 * @property integer $payment_method_id
 * @property integer $order_status_id
 * @property integer $tracking_number
 * @property integer $paid_status
 * @property float $total
 * @property string $comment
 *
 * @property OrderProducts[] $orderProducts
 * @property Delivery $delivery
 * @property OrderStatus $orderStatus
 * @property PaymentMethod $paymentMethod
 * @property Client $client
 */
class Order extends \yii\db\ActiveRecord
{
    const init_tracking_number = 393000;
    public $id_tracking_order = ['5', '6'];
    public $products;
    public $productsForm;

    public function getClient()
    {
        return $this->hasOne(Client::className(), ['id' => 'client_id']);
    }

	public function getProducts()
    {
        $this->products = Product::getOrderProductsById($this->id);
        return $this->products;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['client_id', 'date_book', 'date_create', 'date_update', 'order_status_id', 'paid_status'], 'required'],
            [
                ['client_id', 'delivery_method_id', 'payment_method_id', 'order_status_id', 'order_send_num', 'tracking_number', 'paid_status'],
                'integer'],
            [['total'], 'number'],
            ['order_send_num', 'required',
                'when' => function($model){
                    return $model->order_status_id == 5;
                },
                'whenClient' => "function (attribute, value) {
                    return $('#order-order_status_id').val() == '5';
                }"
            ],
            [['date_send', 'date_end'], 'safe'],
            [['comment'], 'string', 'max' => 255],
            [['products', 'delivery_method_id', 'payment_method_id',], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'client_id' => 'Client ID',
            'date_create' => Yii::t('app', 'Date Create'),
            'date_update' => Yii::t('app', 'Date Update'),
            'date_book' => Yii::t('app', 'Date Booking'),
            'date_end' => Yii::t('app', 'Date End'),
            'date_send' => Yii::t('app', 'Date Send'),
            'delivery_method_id' => Yii::t('app', 'Delivery Method'),
            'payment_method_id' => Yii::t('app', 'Payment Method'),
            'order_status_id' => Yii::t('app', 'Order Status'),
            'order_send_num' => Yii::t('app', 'Order Send Num'),
            'tracking_number' => Yii::t('app', 'Tracking Number'),
            'total' => Yii::t('app', 'Total'),
            'comment' => Yii::t('app', 'Comment'),
            'weight' => Yii::t('app', 'Weight'),
            'paid_status' => Yii::t('app', 'Paid Status'),
        ];
    }

    public static function updateOrderTotalSum($order_id)
    {
        $order = Order::findOne(['id' => $order_id]);
        $totalProductsPrice = (new Query())->from(Product::tableName())->where('order_id = '.$order_id);
        $totalProductsPrice = $totalProductsPrice->sum('total');
        $paymentMarkup = (new Query())
            ->select('markup')
            ->from(PaymentMethod::tableName())
            ->where(['id' => $order->payment_method_id])
            ->scalar();
        if ($paymentMarkup != 0)
        {
            $markUp = ($totalProductsPrice*$paymentMarkup)/100;
            $totalProductsPrice = $totalProductsPrice + $markUp;
        }
        $order->total = $totalProductsPrice;
        $order->save();
        $errors = $order->getErrors();
    }

    /**
     *
     */
    public static function getDeliveryMethodArray()
    {
        $result = (new Query())->select('id, name, free_shipping')->from(Delivery::tableName())->all();
        foreach ($result as $row)
        {
            $arr[$row['id']] = [
                'name' => $row['name'],
                'free_shipping' => $row['free_shipping']
            ];
        }
        return $arr;
    }

    public static function getPaymentMethodArray()
    {
        $result = (new Query())->select('id, name, markup')->from(PaymentMethod::tableName())->all();
        foreach ($result as $row)
        {
            $arr[$row['id']] = [
                'name' => $row['name'],
                'markup' => $row['markup']
            ];
        }
        return $arr;
    }

    public static function getUserListArray()
    {
        $result = User::findAll(['role_id' => 2]);
        foreach ($result as $row)
        {
            if ($row)
                $arr[$row['id']] = $row['username'];
        }
        return $arr;
    }

    public static function getDeliveryMethodArrayForDropDownList()
    {
        $resultArray = Delivery::find()->all();
        foreach ($resultArray as $item)
        {
            $arr[$item['id']] = $item['name'];
        }
        return $arr;
    }

    public static function getPaymentMethodArrayForDropDownList()
    {
        $resultArray = PaymentMethod::find()->all();
        $arr = [];
        foreach ($resultArray as $item)
        {
            if ($item['markup'] > 0)
                $arr[$item['id']] = $item['name'].'(+'.$item['markup'].'% )';
            elseif($item['markup'] < 0)
                $arr[$item['id']] = $item['name'].'('.$item['markup'].'% )';
            else
                $arr[$item['id']] = $item['name'];
        }
        return $arr;
    }

    public static function getOderStatusArrayForDropDownList()
    {
        $resultArray = OrderStatus::find()->all();
        $arr = [];
        foreach ($resultArray as $item)
        {
            $arr[$item['id']] = $item['name'];
        }
        return $arr;
    }

    public static function filterSearch($params)
    {
        $id = $params['client_id'];
        //$query = Address::find()->where(['client_id' => '$id']);
        return new ActiveDataProvider([
            'query' => Order::find()->where(['client_id' => $id]),
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderProducts()
    {
        return $this->hasMany(OrderProducts::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDelivery()
    {
        return $this->hasOne(Delivery::className(), ['id' => 'delivery_method_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderStatus()
    {
        return $this->hasOne(OrderStatus::className(), ['id' => 'order_status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentMethod()
    {
        return $this->hasOne(PaymentMethod::className(), ['id' => 'payment_method_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */

    public function getClientName()
    {
        $model = Client::findOne(['id' => $this->client_id]);
        return $model->name;
    }

    public function beforeSave($insert)
    {
        if (!is_int($this->date_book) && ($this->date_book != 0)) $this->date_book = strtotime($this->date_book);
        if (!is_int($this->date_end) && ($this->date_end != 0)) $this->date_end = strtotime($this->date_end);
        if (!is_int($this->date_send) && ($this->date_send != 0)) $this->date_send = strtotime($this->date_send);
        if (!$this->tracking_number)
        {
            $max = (new Query())
                ->select('tracking_number')
                ->from(Order::tableName())
                ->max('tracking_number');
            $max == null ? $tracking_number_new = self::INIT_TRACKING_NUMBER : $tracking_number_new = $max + 1;
            $this->tracking_number = $tracking_number_new;
        }
        $this->weight = $this->getOrderWeight($this->id);
        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }

    public function getOrderWeight($id)
    {
        $rows = Product::find()->where(['order_id' => $id])->all();
        if (is_array($rows))
        {
            $total = 0;
            foreach ($rows as $row)
            {
                $total += $row['total_weight'];
            }
            return $total;
        }
        return 0;
    }

    public static function getPaidStatusArrayForDropDownList()
    {
        return [
            1 => 'Не оплачен',
            2 => 'Оплачен'
        ];
    }

    public static function getPaidStatusMethodById($id)
    {
        $methodList = self::getPaidStatusArrayForDropDownList();
        if (array_key_exists($id, $methodList))
            return $methodList[$id];
    }

    public static function getPaymentMethodById($id)
    {
        $methodList = self::getPaymentMethodArray();
        if (array_key_exists($id, $methodList))
            return $methodList[$id];
    }


    public static function getDeliveryMethodById($id)
    {
        $methodList = self::getDeliveryMethodArrayForDropDownList();
        if (array_key_exists($id, $methodList))
            return $methodList[$id];
    }

    public static function getOderStatusById($id)
    {
        $methodList = self::getOderStatusArrayForDropDownList();
        if (array_key_exists($id, $methodList))
            return $methodList[$id];
    }
}
