<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "task".
 *
 * @property integer $id
 * @property integer $task_type_id
 * @property integer $client_id
 * @property integer $status
 * @property string $text
 * @property string $date_create
 * @property string $date_end
 */
class Task extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'task';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['task_type_id', 'client_id', 'status'], 'integer'],
            [['text'], 'string'],
            [['task_type_id', 'text', 'status', 'date_end'], 'required'],
            [['date_create'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'task_type_id' => Yii::t('app', 'Task Type ID'),
            'text' => Yii::t('app', 'Text'),
            'status' => Yii::t('app', 'Status'),
            'date_create' => Yii::t('app', 'Date Create'),
            'date_end' => Yii::t('app', 'Date End'),
        ];
    }

    public static function filterSearch($params)
    {
        $id = $params['client_id'];
        //$query = Address::find()->where(['client_id' => '$id']);
        return new ActiveDataProvider([
            'query' => Task::find()->where(['client_id' => $id]),
        ]);
    }

    public static function getTaskTypeArrayForDropDownList()
    {
        $resultArray = TaskType::find()->all();
        $arr = [];
        foreach ($resultArray as $item)
        {
            $arr[$item['id']] = $item['name'];
        }
        return $arr;
    }

    public static function getTaskStatusArrayForDropDownList()
    {
        return [
            '1' => 'Открыта',
            '2' => 'Завершена',
        ];
    }

    public static function getTaskStatusById($id)
    {
        $arr = self::getTaskStatusArrayForDropDownList();
        if (array_key_exists($id, $arr))
            return $arr[$id];
    }

    public static function getTaskTypeById($id)
    {
        $methodList = self::getTaskTypeArrayForDropDownList();
        if (array_key_exists($id, $methodList))
            return $methodList[$id];
    }
}
