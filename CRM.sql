SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `crm` DEFAULT CHARACTER SET utf8 ;
USE `crm` ;

-- -----------------------------------------------------
-- Table `crm`.`address`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `crm`.`address` ;

CREATE  TABLE IF NOT EXISTS `crm`.`address` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `text` TEXT NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `crm`.`role`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `crm`.`role` ;

CREATE  TABLE IF NOT EXISTS `crm`.`role` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(255) NULL DEFAULT NULL ,
  `title` VARCHAR(255) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `crm`.`user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `crm`.`user` ;

CREATE  TABLE IF NOT EXISTS `crm`.`user` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `username` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `email` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `password_hash` VARCHAR(60) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `auth_key` VARCHAR(32) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `confirmed_at` INT(11) NULL DEFAULT NULL ,
  `unconfirmed_email` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  `blocked_at` INT(11) NULL DEFAULT NULL ,
  `registration_ip` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  `created_at` INT(11) NOT NULL ,
  `updated_at` INT(11) NOT NULL ,
  `flags` INT(11) NOT NULL DEFAULT '0' ,
  `role_id` INT(2) NOT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `user_unique_email` (`email` ASC) ,
  UNIQUE INDEX `user_unique_username` (`username` ASC) ,
  INDEX `fk_user_role_id` (`role_id` ASC) ,
  CONSTRAINT `fk_user_role_id`
    FOREIGN KEY (`role_id` )
    REFERENCES `crm`.`role` (`id` )
    ON DELETE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 8
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `crm`.`client`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `crm`.`client` ;

CREATE  TABLE IF NOT EXISTS `crm`.`client` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `user_id` INT(11) NOT NULL ,
  `name` VARCHAR(255) NOT NULL ,
  `email` VARCHAR(64) NULL DEFAULT NULL ,
  `email_2` VARCHAR(64) NULL DEFAULT NULL ,
  `created_at` INT(11) NOT NULL ,
  `updated_at` INT(11) NOT NULL ,
  `phone_work` VARCHAR(64) NULL DEFAULT NULL ,
  `phone_mob` VARCHAR(64) NULL DEFAULT NULL ,
  `phone_home` VARCHAR(64) NULL DEFAULT NULL ,
  `sex` VARCHAR(1) NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_client_user_id` (`user_id` ASC) ,
  CONSTRAINT `fk_client_user_id`
    FOREIGN KEY (`user_id` )
    REFERENCES `crm`.`user` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `crm`.`migration`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `crm`.`migration` ;

CREATE  TABLE IF NOT EXISTS `crm`.`migration` (
  `version` VARCHAR(180) NOT NULL ,
  `apply_time` INT(11) NULL DEFAULT NULL ,
  PRIMARY KEY (`version`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `crm`.`profile`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `crm`.`profile` ;

CREATE  TABLE IF NOT EXISTS `crm`.`profile` (
  `user_id` INT(11) NOT NULL ,
  `name` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  `public_email` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  `gravatar_email` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  `gravatar_id` VARCHAR(32) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  `location` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  `website` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  `bio` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  PRIMARY KEY (`user_id`) ,
  CONSTRAINT `fk_user_profile`
    FOREIGN KEY (`user_id` )
    REFERENCES `crm`.`user` (`id` )
    ON DELETE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `crm`.`social_account`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `crm`.`social_account` ;

CREATE  TABLE IF NOT EXISTS `crm`.`social_account` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `user_id` INT(11) NULL DEFAULT NULL ,
  `provider` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `client_id` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `data` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  `code` VARCHAR(32) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  `created_at` INT(11) NULL DEFAULT NULL ,
  `email` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  `username` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `account_unique` (`provider` ASC, `client_id` ASC) ,
  UNIQUE INDEX `account_unique_code` (`code` ASC) ,
  INDEX `fk_user_account` (`user_id` ASC) ,
  CONSTRAINT `fk_user_account`
    FOREIGN KEY (`user_id` )
    REFERENCES `crm`.`user` (`id` )
    ON DELETE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `crm`.`token`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `crm`.`token` ;

CREATE  TABLE IF NOT EXISTS `crm`.`token` (
  `user_id` INT(11) NOT NULL ,
  `code` VARCHAR(32) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `created_at` INT(11) NOT NULL ,
  `type` SMALLINT(6) NOT NULL ,
  UNIQUE INDEX `token_unique` (`user_id` ASC, `code` ASC, `type` ASC) ,
  CONSTRAINT `fk_user_token`
    FOREIGN KEY (`user_id` )
    REFERENCES `crm`.`user` (`id` )
    ON DELETE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `crm`.`client_addresses`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `crm`.`client_addresses` ;

CREATE  TABLE IF NOT EXISTS `crm`.`client_addresses` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `client_id` INT(11) NOT NULL ,
  `address_id` INT(11) NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_client_address_client1_idx` (`client_id` ASC) ,
  INDEX `fk_client_address_ids_address1_idx` (`address_id` ASC) ,
  CONSTRAINT `fk_client_address_client1`
    FOREIGN KEY (`client_id` )
    REFERENCES `crm`.`client` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_client_address_ids_address1`
    FOREIGN KEY (`address_id` )
    REFERENCES `crm`.`address` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `crm`.`delivery`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `crm`.`delivery` ;

CREATE  TABLE IF NOT EXISTS `crm`.`delivery` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(255) NULL ,
  `free_shipping` INT(11) NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `crm`.`payment_method`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `crm`.`payment_method` ;

CREATE  TABLE IF NOT EXISTS `crm`.`payment_method` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(60) NOT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `crm`.`order_status`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `crm`.`order_status` ;

CREATE  TABLE IF NOT EXISTS `crm`.`order_status` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(255) NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `crm`.`orders`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `crm`.`orders` ;

CREATE  TABLE IF NOT EXISTS `crm`.`orders` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `client_id` INT(11) NOT NULL ,
  `date_create` INT(11) NOT NULL ,
  `date_update` INT(11) NOT NULL ,
  `date_send` INT(11) NULL ,
  `date_end` INT(11) NULL ,
  `delivery_id` INT NOT NULL ,
  `payment_method_id` INT NOT NULL ,
  `order_status_id` INT NOT NULL ,
  `total` INT NULL ,
  `comment` VARCHAR(255) NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_order_client1_idx` (`client_id` ASC) ,
  INDEX `fk_orders_delivery1_idx` (`delivery_id` ASC) ,
  INDEX `fk_orders_payment_method1_idx` (`payment_method_id` ASC) ,
  INDEX `fk_orders_order_status1_idx` (`order_status_id` ASC) ,
  CONSTRAINT `fk_order_client1`
    FOREIGN KEY (`client_id` )
    REFERENCES `crm`.`client` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_orders_delivery1`
    FOREIGN KEY (`delivery_id` )
    REFERENCES `crm`.`delivery` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_orders_payment_method1`
    FOREIGN KEY (`payment_method_id` )
    REFERENCES `crm`.`payment_method` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_orders_order_status1`
    FOREIGN KEY (`order_status_id` )
    REFERENCES `crm`.`order_status` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `crm`.`product`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `crm`.`product` ;

CREATE  TABLE IF NOT EXISTS `crm`.`product` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `code` VARCHAR(45) NOT NULL ,
  `name` VARCHAR(45) NOT NULL ,
  `price` INT NOT NULL DEFAULT 0 ,
  `count` INT NULL DEFAULT 1 ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `crm`.`order_products`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `crm`.`order_products` ;

CREATE  TABLE IF NOT EXISTS `crm`.`order_products` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `order_id` INT NOT NULL ,
  `product_id` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_goods_order1_idx` (`order_id` ASC) ,
  INDEX `fk_order_product_ids_product1_idx` (`product_id` ASC) ,
  CONSTRAINT `fk_goods_order1`
    FOREIGN KEY (`order_id` )
    REFERENCES `crm`.`orders` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_order_product_ids_product1`
    FOREIGN KEY (`product_id` )
    REFERENCES `crm`.`product` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
