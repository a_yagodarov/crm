<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/common.css',
        //'web/css/bootstrap-min.css'
    ];
    public $js = [
//        '/web/js/jquery-ui.js',
        '/web/js/redactor.min.js',
        '/web/js/datepicker.js',
        '/web/js/ru.js',
//        '/web/js/datepicker-ru.js',
        '/web/js/jquery.pjax.js',
        '/web/js/yii.gridView.js',
        '/web/js/bootstrap-formhelpers-number.js',
//        '/web/js/yii.activeForm.js',
    ];
    public $depends = [
//        'yii\widgets\ActiveFormAsset',
//        'yii\validators\ValidationAsset',
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\jui\JuiAsset'
    ];
}
