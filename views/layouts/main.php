<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\models\User;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'Ювелирный дом Апанде',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'id' => 'ww',
            'class' => 'navbar-inverse navbar-relative',
        ],
    ]);

    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right', 'id' => 'ww'],
        'items' => User::getMenuItemsByRoleUser(!Yii::$app->user->isGuest ? Yii::$app->user : false,Yii::$app->user->isGuest)
    ]);
    NavBar::end();
    ?>

    <div class="container" id="content_inner">
        <? \yii\widgets\Pjax::begin(['timeout' => '5000', 'id' => 'pjax_inner'])?>
        <?= Breadcrumbs::widget([
            'homeLink'=>[
                'label' => 'Главная',  // required
                'url' =>  \yii\helpers\Url::home(),      // optional, will be processed by Url::to()
            ],
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ])

        ?>

        <?= $content ?>
        <? \yii\widgets\Pjax::end()?>
    </div>
</div>

<!--<footer class="footer">-->
<!--    <div class="container">-->
<!--        <p class="pull-left">&copy; Material catalog --><?//= date('Y') ?><!--</p>-->
<!---->
<!--        <p class="pull-right">--><?//= Yii::powered() ?><!--</p>-->
<!--    </div>-->
<!--</footer>-->

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
