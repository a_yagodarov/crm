<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
/* @var $this yii\web\View */
/* @var $model app\models\Task */
/* @var $form yii\widgets\ActiveForm */
$js = "
$(document).on('pjax:complete', function() {
console.log('123');
    $('#task-date_end').datepicker();
});
";
$this->registerJs($js, \yii\web\View::POS_END);
?>

<div class="task-form">

    <?php $form = ActiveForm::begin(['options' => ['data-pjax' => true], 'id' => 'task_form_asdas']); ?>

    <?= $form->field($model, 'task_type_id')->dropDownList(\app\models\Task::getTaskTypeArrayForDropDownList(), [
        'prompt' => Yii::t('app', 'Choose task type')
    ])?>

    <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

<!--    --><?//= $form->field($model, 'date_create')->textInput() ?>

    <?= $form->field($model, 'status')->dropDownList(\app\models\Task::getTaskStatusArrayForDropDownList()) ?>


    <?= $form->field($model, 'date_end')->widget(DatePicker::classname(), [
        //'language' => 'ru',
        //'dateFormat' => 'yyyy-MM-dd',
        'options' => ['class' => 'form-control']
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
