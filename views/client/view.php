<?php


use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ListView;
use yii\widgets\LinkPager;
use yii\widgets\Breadcrumbs;
use app\models\Order;

/* @var $this yii\web\View */
/* @var $model app\models\Client */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Clients'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$client_id = $model->id;
if (Yii::$app->request->isAjax)
echo Breadcrumbs::widget([
    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
]);

?>
<div class="client-view">

    <h1><?= Html::encode($this->title) ?></h1>
<div class="row">
    <div class="col-md-4 nopadding">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            [
                'label' => Yii::t('app', 'Gender'),
                'attribute' => 'sex',
                'value' => \app\models\Client::getGenderByKey($model->sex)
                //function($model){
//                    return \app\models\Client::getGenderByKey($model->sex);
//                }
            ],
            'email:email',
            'email_2:email',
//            'created_at:date',
//            'updated_at:date',
            'phone_work',
            'phone_mob',
            'phone_home',
            'comment'
        ],
    ])
    ?>
    <?= Html::a(Yii::t('app', 'Edit'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    <?
    if (Yii::$app->user->identity->role_id == \app\models\User::ROLE_ADMIN)
    echo Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
        'class' => 'btn btn-danger',
        'data-confirm' => 'Вы уверены? Будут удалены также адреса и заказы',
        'data-method' => 'post',
        'data-pjax' => 0,

    ]) ?>
    </div>
    <div class="col-md-8 nopadding">
        <div class="row">
            <h3 style="margin: 0"><?= Yii::t('app', 'Tasks') ?></h3>
            <div style="height: 200px; overflow-y: scroll;">
                <?= \yii\grid\GridView::widget([
                    'dataProvider' => $dataProviderTask,
//                    'filterModel' => $searchModel,
                    'columns' => [
                        [
                            'class' => \yii\grid\ActionColumn::className(),
                            'buttons'=>[
                                'edit'=>function ($url, $model) {
                                    $customurl=Yii::$app->getUrlManager()->createUrl(['task/update','id'=>$model->id]); //$model->id для AR
                                    return \yii\helpers\Html::a( '<span class="glyphicon glyphicon-pencil"></span>', $customurl,
                                        [
                                            'title' => Yii::t('yii', 'Update'),

                                        ]);
                                },
                            ],
                            'template'=>'{edit}',
                        ],
//            'id',
                        [
                            'attribute' => 'task_type_id',
                            'value' => function($model){
                                return (\app\models\Task::getTaskTypeById($model->task_type_id));
                            }
                        ],
                        'text:text',
                        [
                            'attribute' => 'status',
                            'value' => function($model){
//	                            return $model->status;
                                return (\app\models\Task::getTaskStatusById($model->status));
                            }
                        ],
                        'date_create:date',
                        'date_end:date',

                        [
                            'class' => \yii\grid\ActionColumn::className(),
                            'buttons'=>[
                                'delete'=>function ($url, $model) {
                                    $customurl=Yii::$app->getUrlManager()->createUrl(['task/delete','id'=>$model->id]); //$model->id для AR
                                    return \yii\helpers\Html::a( '<span class="glyphicon glyphicon-trash"></span>', $customurl,
                                        [
                                            'title' => Yii::t('yii', 'Delete'),
                                            'data-method' => 'post',
                                            'data-pjax' => 'address_list',

                                            'data-confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                        ]);
                                }
                            ],
                            'template'=>'{delete}'
                        ],
                    ],
                ]); ?>
            </div>
            <br>
            <p>
                <?= Html::a(Yii::t('app', 'Create Task'), ['/task/create', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
            </p>
        </div>
        <div class="row">
            <h3 style="margin: 0"><?= Yii::t('app', 'Addresses') ?></h3>
<!--            --><?// \yii\widgets\Pjax::begin(['id' => 'address_list']) ?>
                <div style="height: 200px; overflow-y: scroll;">
                <?
                    echo \yii\grid\GridView::widget([
                        'dataProvider' => $dataProviderAddress,
                        'columns' => [
                            [
                                'class' => \yii\grid\ActionColumn::className(),
                                'buttons'=>[
                                    'edit'=>function ($url, $model) {
                                        $customurl=Yii::$app->getUrlManager()->createUrl(['address/update','id'=>$model->id]); //$model->id для AR
                                        return \yii\helpers\Html::a( '<span class="glyphicon glyphicon-pencil"></span>', $customurl,
                                            [
                                                'title' => Yii::t('yii', 'Update'),

                                            ]);
                                    },
                                ],
                                'template'=>'{edit}',
                            ],
                            'town',
                            'street',
                            'house',
                            'flat',
                            'porch',
                            'organisation_name',
                            'comment',
                            [
                                'class' => \yii\grid\ActionColumn::className(),
                                'buttons'=>[
                                    'delete'=>function ($url, $model) {
                                        $customurl=Yii::$app->getUrlManager()->createUrl(['address/delete','id'=>$model->id]); //$model->id для AR
                                        return \yii\helpers\Html::a( '<span class="glyphicon glyphicon-trash"></span>', $customurl,
                                            [
                                                'title' => Yii::t('yii', 'Delete'),
                                                'data-method' => 'post',
                                                'data-pjax' => 'address_list',

                                                'data-confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                            ]);
                                    }
                                ],
                                'template'=>'{delete}'
                            ],
                        ],
                        'pager' => [
                            'class' => yii\widgets\LinkPager::className(),
                            'firstPageLabel' => 'first',
                            'lastPageLabel' => 'last',
                            'prevPageLabel' => 'previous',
                            'nextPageLabel' => 'next',
                        ],
                    ])
                ?>
                </div>
<!--            --><?// \yii\widgets\Pjax::end() ?>
            <br>
            <p>
                <?= Html::a(Yii::t('app', 'Create Address'), ['/address/create', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
            </p>
        </div>

        <div class="row">

                <h3 style="margin: 0"><?= Yii::t('app', 'Orders') ?></h3>
                <div style="height: 200px; overflow-y: scroll;">
                <?
                echo \yii\grid\GridView::widget([
                    'dataProvider' => $dataProviderOrder,
                    'pager' => [
                        'class' => yii\widgets\LinkPager::className(),
                        'firstPageLabel' => 'first',
                        'lastPageLabel' => 'last',
                        'prevPageLabel' => 'previous',
                        'nextPageLabel' => 'next',
                    ],
                    'columns' => [
                        [
                            'class' => \yii\grid\ActionColumn::className(),
                            'buttons'=>[
                                'edit'=>function ($url, $model) {
                                    $customurl=Yii::$app->getUrlManager()->createUrl(['order/update','id'=>$model->id]); //$model->id для AR
                                    return \yii\helpers\Html::a( '<span class="glyphicon glyphicon-pencil"></span>', $customurl,
                                        [
                                            'title' => Yii::t('yii', 'Update')
                                        ]);
                                },
                            ],
                            'template'=>'{edit}',
                        ],
//                        'date_create:date',
//                        'date_update',
                        'order_send_num',
                        'tracking_number',
                        'date_book:date',
//                         'date_end:date',
                         [
                             'attribute' => 'delivery_method_id',
                             'value' => function ($model){
                                 return \app\models\Delivery::findOne(['id' => $model->delivery_method_id])->name;
                             }
                         ],
                        [
                            'attribute' => 'payment_method_id',
                            'value' => function ($model){
                                return \app\models\PaymentMethod::findOne(['id' => $model->payment_method_id])->name;
                            }
                        ],
                        [
                            'attribute' => 'order_status_id',
                            'value' => function ($model){
                                return \app\models\OrderStatus::findOne(['id' => $model->order_status_id])->name;
                            }
                        ],
                        [
                            'attribute' => 'paid_status',
                            'value' => function ($model){
                                return Order::getPaidStatusMethodById($model->paid_status);
                            }
                        ],
                        'weight',
                        'total',
                         //'comment',
                        [
                            'class' => \yii\grid\ActionColumn::className(),
                            'buttons'=>[
                                'delete'=>function ($url, $model) {
                                    $customurl=Yii::$app->getUrlManager()->createUrl(['order/delete','id'=>$model->id]); //$model->id для AR
                                    return \yii\helpers\Html::a( '<span class="glyphicon glyphicon-trash"></span>', $customurl,
                                        [
                                            'title' => Yii::t('yii', 'Delete'),
                                            'data-method' => 'post',
                                            'data-pjax' => 0,
                                            'data-confirm' => Yii::t('app', 'Are you sure you want to delete this item?'
                                )]);
                                }
                            ],
                            'template'=>'{delete}',
                        ],
                    ],
                    'pager' => [
                        'class' => yii\widgets\LinkPager::className(),
                        'firstPageLabel' => 'first',
                        'lastPageLabel' => 'last',
                        'prevPageLabel' => 'previous',
                        'nextPageLabel' => 'next',
                    ],
                ])
                ?>
            </div>

            <p>
                <?= Html::a(Yii::t('app', 'Create Order'), ['/order/create', 'client_id' => $client_id], ['class' => 'btn btn-success']) ?>
            </p>
        </div>
    </div>
</div>
<!--    <div id="myCarousel" class="carousel slide" data-ride="carousel">-->
<!--        <!-- Indicators -->-->
<!--        <ol class="carousel-indicators">-->
<!--            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>-->
<!--            <li data-target="#myCarousel" data-slide-to="1"></li>-->
<!--            <li data-target="#myCarousel" data-slide-to="2"></li>-->
<!--            <li data-target="#myCarousel" data-slide-to="3"></li>-->
<!--        </ol>-->
<!---->
<!--        <!-- Wrapper for slides -->-->
<!--        <div class="carousel-inner" role="listbox">-->
<!--            <div class="item active">-->
<!--                <img src="img_chania.jpg" alt="Chania">-->
<!--            </div>-->
<!---->
<!--            <div class="item">-->
<!--                <img src="img_chania2.jpg" alt="Chania">-->
<!--            </div>-->
<!---->
<!--            <div class="item">-->
<!--                <img src="img_flower.jpg" alt="Flower">-->
<!--            </div>-->
<!---->
<!--            <div class="item">-->
<!--                <img src="img_flower2.jpg" alt="Flower">-->
<!--            </div>-->
<!--        </div>-->
<!---->
<!--        <!-- Left and right controls -->-->
<!--        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">-->
<!--            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>-->
<!--            <span class="sr-only">Previous</span>-->
<!--        </a>-->
<!--        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">-->
<!--            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>-->
<!--            <span class="sr-only">Next</span>-->
<!--        </a>-->
<!--    </div>-->

</div>