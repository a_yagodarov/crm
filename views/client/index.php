<?php

use yii\helpers\Html;
use yii\grid\GridView;
use \yii\widgets\ListView;
use \yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ClientSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = Yii::t('app', 'Clients');
$this->params['breadcrumbs'][] = ['label' => $this->title];
$ajaxUrl = json_encode(\yii\helpers\Url::to('client/render'));
if (Yii::$app->request->isAjax)
    echo \yii\widgets\Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]);
$url = Url::to('/client/render');
$script = <<< JS
function getClientList(method)
{
    var formData = $('form#wddf').serialize();
    console.log(formData);
    $.ajax({
               url: '/client/render',
               type: method,
               data: formData,
               success: function (data) {
                   //console.log(data);
                  $('#clients_list').empty();
                  $('#clients_list').append(data);
               }
          });
}

function addAjaxIntoInput(){
    console.log('addAjax');
    $('#wddf #reset').on('click', function(){
        $('#wddf input').val('');
        getClientList('post');
    });
    $('input').on('input', function(){
        getClientList('post');
    });
    $('input').on('change', function(){
        getClientList('post');
    });
    $('input[type="checkbox"]').on('change', function(){
        getClientList('post');
    });
    $('form #submit').on('click', function(e){
        e.preventDefault();
        getClientList('post');
    })
}
addAjaxIntoInput();
JS;
$this->registerJs($script, \yii\web\View::POS_END);
?>
<?

?>
<div class="client-index">
    <div class="col-xs-2">
        <h3 style="margin-top:0px"><?= Yii::t('app', 'Search title') ?></h3>
        <div class="row-fluid">
            <?= $this->render('_search', [
                'model' => $searchModel
            ])?>
        </div>
    </div>
    <div class="col-xs-10" id="clients_list">
        <?
        echo $this->render('_client_list', [
            'dataProvider' => $dataProvider
        ]);
        ?>
    </div>

</div>