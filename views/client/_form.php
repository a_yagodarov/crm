<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \app\models\Client;
/* @var $this yii\web\View */
/* @var $model app\models\Client */
/* @var $form yii\widgets\ActiveForm */

?>


<div class="client-form">

    <?php $form = ActiveForm::begin([
            'id' => 'wwwz',
		    'options' => ['data-pjax' => true]
//            'enableAjaxValidation' => true,
        ]
    ); ?>

<!--    --><?//= $form->field($model, 'user_id')->textInput() ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?
        if($model->sex)
        {
            echo $form
                ->field($model, 'sex')
                ->dropDownList(Client::getArrayGender());
        }
        else
        {
            echo $form
                ->field($model, 'sex')
                ->dropDownList(Client::getArrayGender(),['prompt' => Yii::t('app', 'Choose gender'), 'class'=>'form-control']);
        }
    ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email_2')->textInput(['maxlength' => true]) ?>
<!---->
<!--    --><?//= $form->field($model, 'created_at')->textInput() ?>
<!---->
<!--    --><?//= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'phone_work')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone_mob')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone_home')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'comment')->textarea(['rows' => 6, 'maxlength' => true]) ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), [
            'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary',
            'data-pjax' => 0
        ]) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
