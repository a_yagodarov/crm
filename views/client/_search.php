<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ClientSearch */
/* @var $form yii\widgets\ActiveForm */

$script = <<< JS

JS;

$this->registerJs($script);
?>

<div class="client-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => ['data-pjax' => true],
        'id' => 'wddf'
    ]); ?>

<!--    --><?//= $form->field($model, 'id') ?>

<!--    --><?//= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'email') ?>

    <?= $form->field($model, 'tracking_number') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php  echo $form->field($model, 'phone_work')->label(Yii::t('app', 'Phone')) ?>

<!--    --><?php // echo $form->field($model, 'phone_mob') ?>
<!---->
<!--    --><?php // echo $form->field($model, 'phone_home') ?>

    <?php  echo $form->field($model, 'order_send_num') ?>
    <?php  echo $form->field($model, 'active_tasks')->checkbox() ?>
    <?= $form->field($model, 'task_date_end')->widget(\yii\jui\DatePicker::classname(), [
        //'language' => 'ru',
        //'dateFormat' => 'yyyy-MM-dd',
        'options' => ['class' => 'form-control']
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['id' => 'submit', 'class' => 'btn btn-primary']) ?>
        <?= Html::Button(Yii::t('app', 'Reset'), ['id' => 'reset', 'class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
