<?php

use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $model app\models\Client */

$this->title = Yii::t('app', 'Edit client profile').': ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Clients'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Edit');

if (Yii::$app->request->isAjax)
    echo \yii\widgets\Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]);
?>
<div class="client-update">

    <h3><?= Html::encode($this->title) ?></h3>
    <div class="col-xs-12">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
    </div>
</div>

<?
?>
