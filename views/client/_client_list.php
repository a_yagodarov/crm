<?php
/**
 * Created by PhpStorm.
 * User: Алексей
 * Date: 03.03.2016
 * Time: 16:20
 */
use yii\widgets\ListView;
use yii\helpers\Html;
$this->title = Yii::t('app', 'Clients');

$script = <<< JS
window.addEventListener('popstate', function(e) {
    addClickLinkListener()
});
//addAjaxIntoInput();
function addClickLinkListener(){
$('#list-wrapper .pagination a').click(function(e){
    e.preventDefault();
    var link = e.target.href;
    str = link.split('page=');
    page = str[1];
    console.log(page);
    var formData = $('form#wddf').serialize();
    console.log(formData);
    if (page)
    formData += '&page='+page;
        $.ajax({
                   url: '/client/render?'+formData,
                   type: 'post',
                   success: function (data) {
                       //console.log(data);
                      $('#clients_list').empty();
                      $('#clients_list').append(data);
                   }
              });
});}
addClickLinkListener();
JS;

$this->registerJs($script, \yii\web\View::POS_END);
?>
<h3 style="margin-top:0px"><?= Html::encode($this->title) ?></h3>
<div class="row-fluid">
    <p>
        <?= Html::a(Yii::t('app', 'Create client'), ['create'], ['class' => 'btn btn-success', 'id' => 'create_client']) ?>
    </p>
</div>
<?
echo ListView::widget([
    'dataProvider' => $dataProvider,
    'options' => [
        'tag' => 'div',
        'class' => 'list-wrapper',
        'id' => 'list-wrapper',
    ],

    'layout' => "{summary}\n{pager}\n{items}",
    'itemView' => function ($model, $key, $index, $widget) {
        return Html::a($model->name, ['/client/view', 'id' => $model->id], ['class'=>'btn btn-default']);
        //return Html::a('button', $model->name, ['type' => 'button', 'class' => 'btn btn-sm btn-default', 'id' => $model->id]);
    }
]);
?>