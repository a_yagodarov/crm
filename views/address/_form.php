<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use demogorgorn\ajax\AjaxSubmitButton;
/* @var $this yii\web\View */
/* @var $model app\models\Address */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="address-form">

    <?php $form = ActiveForm::begin(['options' => ['data-pjax' => true],'id' => 'wx']); ?>
<!---->
<!--    --><?//= $form->field($model, 'client_id')->textInput() ?>

<!--    --><?//= $form->field($model, 'client_id')->textInput() ?>
    <?= $form->field($model, 'town')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'street')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'house')->textInput() ?>
    <?= $form->field($model, 'flat')->textInput() ?>
    <?= $form->field($model, 'porch')->textInput() ?>
    <?= $form->field($model, 'organisation_name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'comment')->textarea(['rows' => 6, 'maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
<!--        --><?//
//        AjaxSubmitButton::begin([
//            'label' => 'Сохранить',
//            'ajaxOptions' => [
//                'type' => 'POST',
//                'url' => \yii\helpers\Url::toRoute('/address/create'),
//                'success' => new \yii\web\JsExpression('function(responseText){
//                        $("#container").empty();
//                        $("#container").append(responseText);
//                    }'),
//            ],
//            'options' => ['id' => 'wx', 'class' => 'btn btn-success', 'type' => 'submit'],
//        ]);
//        AjaxSubmitButton::end();
//        ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
