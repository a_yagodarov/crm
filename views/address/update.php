<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Address */
use yii\widgets\Breadcrumbs;
$client = $model->getClient()->one();
$this->title = Yii::t('app', 'Update Address');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Clients'), 'url' => ['/client/index']];
$this->params['breadcrumbs'][] = ['label' => $client->name, 'url' => ['client/view', 'id' => $model->client_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');

if (Yii::$app->request->isAjax)
    echo Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]);
?>
<div class="address-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
