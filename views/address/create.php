<?php

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $model app\models\Address */
$client = \app\models\Client::findOne(['id' => Yii::$app->request->get('id')]);
$this->title = Yii::t('app', 'Create {modelClass}: ', [
        'modelClass' => 'Address',
    ]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Clients'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $client->name, 'url' => ['client/view', 'id' => $client->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Create {modelClass}: ', [
    'modelClass' => 'Address',
]);

if (Yii::$app->request->isAjax)
    echo Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]); ?>
<div class="address-create">
    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
