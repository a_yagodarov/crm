<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ProductTemp */

$this->title = Yii::t('app', 'Create Product Temp');
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Product Temps'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
if (Yii::$app->request->isAjax)
    echo \yii\widgets\Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]);

?>
<div class="product-temp-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
