<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
use vova07\imperavi\Widget;
/* @var $this yii\web\View */
/* @var $model app\models\Order */
/* @var $form yii\widgets\ActiveForm */

$script = <<< JS
var order_status_sended = 5;
var order_status_last = $('#order-order_status_id').val();
$('#order-order_send_num').on('input', function(){
    if ($('#order-order_send_num').val() != '')
    {
        $('#order-order_status_id').val(order_status_sended);
    }
    else
    {
        $('#order-order_status_id').val(order_status_last);
    }
});
JS;

$this->registerJs($script, yii\web\View::POS_READY);
?>

<div class="order-form">

    <?php $form = ActiveForm::begin(['options' => ['data-pjax' => true],'id' => 'wd']); ?>

    <?= $form->field($model, 'date_book')->widget(DatePicker::classname(), [
        //'language' => 'ru',
        //'dateFormat' => 'yyyy-MM-dd',
        'options' => ['class' => 'form-control']
    ]) ?>

    <?= $form->field($model, 'date_send')->widget(DatePicker::classname(), [
        //'language' => 'ru',
        //'dateFormat' => 'yyyy-MM-dd',
        'options' => ['class' => 'form-control']
    ]) ?>

    <?= $form->field($model, 'date_end')->widget(DatePicker::classname(), [
        //'language' => 'ru',
        //'dateFormat' => 'yyyy-MM-dd',
        'options' => ['class' => 'form-control']
    ]) ?>
    <?= $form->field($model, 'delivery_method_id')
        ->dropDownList(
            \app\models\Order::getDeliveryMethodArrayForDropDownList(),
            [
                'prompt' => Yii::t('app', 'Choose delivery method')
            ]
            ) ?>

    <?= $form->field($model, 'payment_method_id')
        ->dropDownList(\app\models\Order::getPaymentMethodArrayForDropDownList(),
            [
                'prompt' => Yii::t('app', 'Choose payment method')
            ]) ?>

    <?= $form->field($model, 'order_status_id')
        ->dropDownList(\app\models\Order::getOderStatusArrayForDropDownList(),
            [
                'prompt' => Yii::t('app', 'Choose order status')
            ]) ?>

    <?= $form->field($model, 'paid_status')
        ->dropDownList(\app\models\Order::getPaidStatusArrayForDropDownList()) ?>

    <?= $form->field($model, 'order_send_num')->textInput();?>

    <?= $form->field($model, 'comment')->textarea(['rows' => 6, 'maxlength' => true]) ?>

    <?
        echo $this->render('_product_form', [
            'productModel' => $model->productsForm,
            'orderProducts' => $model->products,
            'client_id' => $model->id
        ]);
    ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success center-block ' : 'btn btn-primary center-block']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
