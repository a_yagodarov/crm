<?php
/**
 * Created by PhpStorm.
 * User: Алексей
 * Date: 28.02.2016
 * Time: 11:45
 */
use yii\helpers\Html;
\yii\widgets\Pjax::begin(['id' => 'client_products_gridview']);
?>
<h3>Список товаров:</h3>
    <?= \yii\grid\GridView::widget([
    'dataProvider' => $dataProvider,
    //'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        'code',
        'name',
        'price',
        'provider',
        'provider_price',
        'count',
        'total',
        [
            'class' => \yii\grid\ActionColumn::className(),
            'buttons'=>[
                'view'=>function ($url, $model) {
                    $customurl=Yii::$app->getUrlManager()->createUrl(['producttemp/view','id'=>$model->id]); //$model->id для AR
                    return \yii\helpers\Html::a( '<span class="glyphicon glyphicon-eye-open"></span>', $customurl,
                        ['title' => Yii::t('yii', 'View')]);
                },
                'edit'=>function ($url, $model) {
                    $customurl=Yii::$app->getUrlManager()->createUrl(['producttemp/update','id'=>$model->id]); //$model->id для AR
                    return \yii\helpers\Html::a( '<span class="glyphicon glyphicon-pencil"></span>', $customurl,
                        ['title' => Yii::t('yii', 'Update')]);
                },
                'delete'=>function ($url, $model) {
                    $customurl=Yii::$app->getUrlManager()->createUrl(['producttemp/delete','id'=>$model->id]); //$model->id для AR
                    return \yii\helpers\Html::a( '<span class="glyphicon glyphicon-trash"></span>', $customurl,
                        ['title' => Yii::t('yii', 'Delete')]);
                }
            ],
            'template'=>'{view} {edit} {delete}',
        ],
    ],
]);
    \yii\widgets\Pjax::end();?>
<h3><?
        echo Html::a(Yii::t('app', 'Create Product Temp'), ['/producttemp/create', 'id' => $client_id]);
    ?>
</h3>