<?php
/**
 * Created by PhpStorm.
 * User: Алексей
 * Date: 28.02.2016
 * Time: 11:45
 */
use kartik\grid\GridView;
use yii\helpers\Html;
use \yii\widgets\Pjax;
Pjax::begin(['id' => 'client_products_gridview']);
?>
<h3>Список товаров:</h3>
    <?= GridView::widget([
    'dataProvider' => $dataProvider,
    //'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        'code',
        'name',
        'price',
        'provider',
        'provider_price',
        'count',
        'total',
        [
            'class' => \yii\grid\ActionColumn::className(),
            'buttons'=>[
                'edit'=>function ($url, $model) {
                    $customurl=Yii::$app->getUrlManager()->createUrl(['product/update','id'=>$model->id]); //$model->id для AR
                    return \yii\helpers\Html::a( '<span class="glyphicon glyphicon-pencil"></span>', $customurl,
                        ['title' => Yii::t('yii', 'Update')]);
                },
                'delete'=>function ($url, $model) {
                    $customurl=Yii::$app->getUrlManager()->createUrl(['product/delete','id'=>$model->id]); //$model->id для AR
                    return \yii\helpers\Html::a( '<span class="glyphicon glyphicon-trash"></span>', $customurl,
                        ['title' => Yii::t('yii', 'Delete')]);
                }
            ],
            'template'=>'{edit} {delete}',
        ],
    ],
]);
Pjax::end();
    ?>
<h3><?
        echo Html::a(Yii::t('app', 'Create Product Temp'), ['/product/create', 'client_id' => $client_id]);
    ?>
</h3>