<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Order */
$client = $model->getClient()->one();
$model->isNewRecord ? $titleAction =  Yii::t('app', 'Create order of user') : $titleAction = Yii::t('app', 'Update order of user');
$this->title = $titleAction.' '.$model->getClientName();
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Orders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Clients'), 'url' => ['/client/index']];
$this->params['breadcrumbs'][] = ['label' => $client->name, 'url' => ['client/view', 'id' => $model->client_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
if (Yii::$app->request->isAjax)
    echo \yii\widgets\Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]);


?>
<div class="order-update">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
        'orderProducts' => $orderProducts,
        'dataProvider' => $dataProvider,
        'searchModel' => $searchModel
    ]) ?>

</div>
