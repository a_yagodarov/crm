<?php
/**
 * Created by PhpStorm.
 * User: Алексей
 * Date: 21.02.2016
 * Time: 17:15
 */
use yii\helpers\Html;

$payment = json_encode(app\models\Order::getPaymentMethodArray());
$delivery = json_encode(app\models\Order::getDeliveryMethodArray());

$script = <<< JS
$(document).ready(function(){
payment = $payment;
delivery = $delivery;
function upNameOfInput(row){
    row = row.toString();
    num = parseInt(/\d+/.exec(row));
    num = num + 1;
    row = row.replace(/\d+/, num);
    return row;
};
function addWeightChangeListener()
{
    $('.weight').change(function(e){
        var weight = $(this).val();
        var count = $(this).parent().parent().find('.count').val();
        console.log(weight+' '+count);
        $(this).parent().parent().find('.total_weight').val(count*weight);
        updateTotalWeight()
    })
}
function addPriceChangeListener()
{
    $('.price').change(function(e){
        var price = $(this).val();
        var count = $(this).parent().parent().find('.count').val();
        $(this).parent().parent().find('.total').val(count*price);
        updateTotalPrice();
    });
}
function addCountChangeListener()
{
    $('.count').change(function(e){
        var count = $(this).val();
        var price = $(this).parent().parent().find('.price').val();
        var weight = $(this).parent().parent().find('.weight').val();
        $(this).parent().parent().find('.total').val(count*price);
        updateTotalPrice();
        $(this).parent().parent().find('.total_weight').val(count*weight);
        updateTotalWeight();
    });
}
function addRemoveEvent()
{
    $('#products-table > tbody button').click(function(){
        $(this).parent().parent().remove();
    });
}
function showWarningBorderEmptyinput()
{
$('[type=string]').each(function()
{
    if ($(this).val() == '')
        $(this).css('border-color', '#f0ad4e');
    else
        $(this).css('border-color', '#ccc')})
}
function updateTotalWeight()
{
    var sum = 0;
    $('.total_weight').each(function(){
        sum += Number($(this).val());
        console.log();
    });
    $('#total_weight').val(sum);
}
function updateTotalPrice()
{
    var sum = 0;
    $('.total').each(function(){
        sum += Number($(this).val());
    });
    sum += getPaymentMethodPrice(sum);
    console.log('sum = '+sum);
    console.log('надбавка = '+getPaymentMethodPrice(sum));
    $('#total').val(sum);
}
function getPaymentMethodPrice(sum)
{
    selected = $("[name='Order[payment_method_id]']").val();
    if (selected == 0)
    {
        $('#payment').val(0);
        return selected;
    }
    value = Number(payment[selected]['markup']);
    console.log(value);
    markup = ((sum * value)/100);
    $('#payment').val(markup);
    console.log(markup);
    return markup;
}
function setDefaultValues()
{
    $('form .bfh-number').each(function () {
      var number;

      number = $(this);

      number.bfhnumber(number.data());
    });
}
function updateTable()
{
    updateTotalPrice();
    setDefaultValues();
    showWarningBorderEmptyinput();
    addRemoveEvent();
    addPriceChangeListener();
    addCountChangeListener();
    addWeightChangeListener();
    updateTotalWeight()
}
selector = '#products-table > tbody > tr:last-child';
productRowTemp = $(selector);
$('#addRow').click(function(){
    productRowForm = $(selector).clone();
    //productRowForm.each(function(){
    //    console.log('1'+$(this)+'<br>');
    //    $(this).attr('input', 'name', upNameOfInput());
    //});
    if(tbody = $('#products-table > tbody'))
    {
        tbody.append(productRowForm);
        addedRowForm = $(selector);
        $(selector + ' td input').val('');
        $(selector + ' td:first-child').html(upNameOfInput($(selector + ' td:first-child').html()));
        $(selector + ' input').each(function(){
            attr = $(this).attr('name');
            $(this).attr('name', upNameOfInput(attr));
        });
        upNameOfInput(addedRowForm);
        updateTable();
    }
})
$("[name='Order[payment_method_id]").change(function()
{
    updateTotalPrice();
});
$("[type=string]").change(function(){
    showWarningBorderEmptyinput();
});
updateTable()
})
JS;

$this->registerJs($script, yii\web\View::POS_READY);

?>
<h3>Список товаров:</h3>
<div class="row-fluid">
    <div class="row">
        <table class="table table-bordered products" id="products-table">
            <thead>
            <tr>
            <?
                echo '<th>№</th>';
                foreach ($productModel as $column)
                {
                    if ($column['name'] != 'id')
                        echo '<th>'.Yii::t('app', $column['name']).'</th>';
                }
            ?>
            </tr>
            </thead>

            <tbody>
                <?
                $i = 0;
                if (!empty($orderProducts))
                {
                    foreach ($orderProducts as $orderProduct)
                    {
                        echo '<tr>';
                        echo '<td>'.$i.'</td>';
                            foreach ($orderProduct as $product) {
                                if ($product['name'] != 'id') {
                                    $cell = '';
                                    $cell .= '<td>';
                                    if ($product['name'] == 'total' || $product['name'] == 'total_weight')
                                    {
                                        $cell .= Html::input(
                                            $product['type'],
                                            'model[products][' . $i . '][' . $product['name'] . ']',
                                            ($product['value'] != null || $product['value'] != '') ? $product['value'] : $product['defaultValue'],
                                            [
                                                'class' => 'form-control bfh-number '.$product['name'],
                                                'readonly' => ''
                                            ]
                                        );
                                    }
                                    else
                                    {
                                        $cell .= Html::input(
                                            $product['type'] == 'integer' ? 'text' : 'string',
                                            'model[products][' . $i . '][' . $product['name'] . ']',
                                            ($product['value'] != null || $product['value'] != '') ? $product['value'] : $product['defaultValue'],
                                            $product['type'] == 'integer' ?
                                                [
                                                    'class' => 'form-control bfh-number '.$product['name'],
                                                ] :
                                                [
                                                    'class' => 'form-control '.$product['name'],
                                                ]
                                        );
                                    }
                                    $cell .= '</td>';
                                    echo $cell;
                                }
                                else
                                    echo Html::hiddenInput('model[products][' . $i . '][id]', $product['value']);
                            }
                ?>
                            <td>
                                <button type="button" class="btn btn-default">
                                    <span type="button" class="glyphicon glyphicon-trash"></span>
                                </button>
                            </td>
                        </tr>
                <?
                        $i++;
                    }
                }
                ?>
                <?
                    for ($j = $i; $j < $i + 1; $j++) {
                        echo '<tr>';
                        echo '<td>'.$j.'</td>';
                            foreach ($productModel as $product) {
                                if ($product['name'] != 'id') {
                                    $cell = '';
                                    $cell .= '<td>';
                                    if ($product['name'] == 'total' || $product['name'] == 'total_weight')
                                    {
                                        $cell .= Html::input(
                                            $product['type'],
                                            'model[products][' . $j . '][' . $product['name'] . ']',
                                            ($product['value'] != null || $product['value'] != '') ? $product['value'] : $product['defaultValue'],
                                            [
                                                'class' => 'form-control bfh-number '.$product['name'],
                                                'readonly' => ''
                                            ]
                                        );
                                    }
                                    else
                                    {
                                        $cell .= Html::input(
                                            $product['type'] == 'integer' ? 'text' : 'string',
                                            'model[products][' . $j . '][' . $product['name'] . ']',
                                            ($product['value'] != null || $product['value'] != '') ? $product['value'] : $product['defaultValue'],
                                            $product['type'] == 'integer' ?
                                                [
                                                    'class' => 'form-control bfh-number '.$product['name'],
                                                ] :
                                                [
                                                    'class' => 'form-control '.$product['name'],
                                                ]
                                        );
                                    }
                                    $cell .= '</td>';
                                    echo $cell;
                                }
                                else
                                    echo Html::hiddenInput('model[products][' . $j . '][id]', $product['value']);
                            }
                        ?>
                            <td>
                                <button type="button" class="btn btn-default">
                                    <span type="button" class="glyphicon glyphicon-trash"></span>
                                </button>
                            </td>
                        </tr>
                <?
                    }
                $x = 1;
                $_POST['x'] = $x;
                ?>
            </tbody>
        </table>
    </div>
    <div class="row">
        <div class="col-xs-2">
            <button class="bt btn-primary" type="button" id="addRow">Добавить строку</button>
            </br>
        </div>
        <div class="col-xs-4 col-xs-offset-6" >
            <table>
                <tbody>
                <tr>
                    <td>
                        Сумма по методу оплаты(руб.):
                    </td>
                    <td>
                        <input type="integer" class="form-control" id="payment" readonly="">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Общая сумма(руб.):</label>
                    </td>
                    <td>
                        <input type="integer" class="form-control" id="total" readonly="">
                    </td>
                </tr>
                <tr>
                    <td>
                        Общий вес(гр.)
                    </td>
                    <td>
                        <input type="integer" class="form-control" id="total_weight" readonly="">
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>