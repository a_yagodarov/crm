<?php

use yii\helpers\Html;

use kartik\grid\GridView;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Orders');
$this->params['breadcrumbs'][] = $this->title;
if (Yii::$app->request->isAjax)
    echo \yii\widgets\Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]);
$url = \yii\helpers\Url::to('/order/index');
$css = <<< CSS
.pagination {

}
CSS;

$script = <<< JS
var url = '$url';
var data = '?';
var selector = '#w0-filters td';
//function serializeDiv()
//{
//	return getUrl();
//}
function getInputData()
{
    if ((($(this).children()).serialize()).length == 0)
    {
    	console.log($(this));
        return '';
    }
    if (i == 1)
    {
        i ++;
        return (($(this).children()).serialize());
    }
    return '&'+(($(this).children()).serialize())
}
function getUrl()
{
    $(selector).each(
    function(){
        data += getInputData();
    })
    console.log(url+data);
    return url+data;
}
function getClientList(method)
{
    formData = getUrl();
    $.ajax({
               url: '/order/index',
               type: method,
               data: formData,
               success: function (data) {
                  $('#w0-container tbody').empty();
                  $('#w0-container table').append($(data).find('tbody'));
               }
          });
}

function addAjaxIntoInput(){
    console.log('addAjax');
    $('#wddf #reset').on('click', function(){
        $('#wddf input').val('');
        getClientList('post');
    });
    $('input').on('input', function(){
        getClientList('post');
    });
    $('input').on('change', function(){
        getClientList('post');
    });
    $('input[type="checkbox"]').on('change', function(){
        getClientList('post');
    });
    $('form #submit').on('click', function(e){
        e.preventDefault();
        getClientList('post');
    })
}
addAjaxIntoInput();
$('.pagination').addClass('hidden');
JS;
$this->registerJs($script, \yii\web\View::POS_END);
?>
<div class="order-index">

    <h1><?= Html::encode($this->title) ?></h1>

<!--    <p>-->
<!--        --><?//= Html::a(Yii::t('app', 'Create Order'), ['create'], ['class' => 'btn btn-success']) ?>
<!--    </p>-->

<!--    --><?//= $this->render('_search', [
//        'model' => $searchModel
//    ])?>
<?// \yii\widgets\Pjax::begin(['id' => 'gridview_wawasasd'])?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'layout' => '{items}{pager}',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            [
                'attribute' => 'client_id',
                'label' => Yii::t('app', 'Client'),
                'format' => 'raw',
                'value' => function($model){
                    $name = \app\models\Client::findOne(['id' => $model->client_id])->name;
                    $url = \yii\helpers\Url::to(['client/view', 'id' => $model->client_id]);
                    return Html::a($name, $url);
                },
            ],
            [
                'attribute' => 'delivery_method_id',
//                'label' => 'Delivery Method',
                'value' => function($model)
                {
                    return  \app\models\Order::getDeliveryMethodById($model->delivery_method_id);
                },
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'delivery_method_id',
                    \app\models\Order::getDeliveryMethodArrayForDropDownList(),
                    ['class' => 'form-control', 'prompt' => Yii::t('app', 'Choose delivery method')]
                )
            ],
            [
                'attribute' => 'payment_method_id',
                'value' => function($model)
                {
                    $name = \app\models\Order::getPaymentMethodById($model->payment_method_id)['name'];
                    return $name;
                },
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'payment_method_id',
                    \app\models\Order::getPaymentMethodArrayForDropDownList(),
                    ['class' => 'form-control', 'prompt' => Yii::t('app', 'Choose payment method')]
                )
            ],
            [
                'attribute' => 'order_status_id',
                'value' => function($model)
                {
                    $status = $model->order_status_id;
                    $name = \app\models\Order::getOderStatusById($status);
                    return $name;
                },
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'order_status_id',
                    \app\models\Order::getOderStatusArrayForDropDownList(),
                    ['class' => 'form-control', 'prompt' => Yii::t('app', 'Choose order status')]
                )
            ],
            'tracking_number',
             'order_send_num',
             'total',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}'
            ],
        ],
    ]); ?>
<?// \yii\widgets\Pjax::end()?>
</div>
