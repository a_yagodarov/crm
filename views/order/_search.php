<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use yii\jui\DatePicker;
/* @var $this yii\web\View */
/* @var $model app\models\OrderSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'type' => ActiveForm::TYPE_INLINE
    ]); ?>
<!--    --><?//= $form->field($model, 'name');?>

<!--    --><?//= $form->field($model, 'date_book')->widget(DatePicker::classname(), [
//        //'language' => 'ru',
//        //'dateFormat' => 'yyyy-MM-dd',
//        'options' => ['class' => 'form-control']
//    ]) ?>
<!---->
<!--    --><?//= $form->field($model, 'date_send')->widget(DatePicker::classname(), [
//        //'language' => 'ru',
//        //'dateFormat' => 'yyyy-MM-dd',
//        'options' => ['class' => 'form-control']
//    ]) ?>
<!---->
<!--    --><?//= $form->field($model, 'date_end')->widget(DatePicker::classname(), [
//        //'language' => 'ru',
//        //'dateFormat' => 'yyyy-MM-dd',
//        'options' => ['class' => 'form-control']
//    ]) ?>
    <?= $form->field($model, 'delivery_method_id')
        ->dropDownList(
            \app\models\Order::getDeliveryMethodArrayForDropDownList(),
            [
                'prompt' => Yii::t('app', 'Choose delivery method')
            ]
        ) ?>

    <?= $form->field($model, 'payment_method_id')
        ->dropDownList(\app\models\Order::getPaymentMethodArrayForDropDownList(),
            [
                'prompt' => Yii::t('app', 'Choose payment method')
            ]) ?>

    <?= $form->field($model, 'order_status_id')
        ->dropDownList(\app\models\Order::getOderStatusArrayForDropDownList(),
            [
                'prompt' => Yii::t('app', 'Choose order status')
            ]) ?>

    <?= $form->field($model, 'order_send_num')->textInput();?>
    <?= $form->field($model, 'tracking_number')->textInput();?>
    <?= $form->field($model, 'total')->textInput();?>

<!---->
<!--    --><?php // echo $form->field($model, 'delivery_method_id') ?>

    <?php // echo $form->field($model, 'payment_method_id') ?>

    <?php // echo $form->field($model, 'order_status_id') ?>

    <?php // echo $form->field($model, 'total') ?>

    <?php // echo $form->field($model, 'comment') ?>

    <?php // echo $form->field($model, 'order_send_num') ?>

    <?php // echo $form->field($model, 'tracking_number') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
