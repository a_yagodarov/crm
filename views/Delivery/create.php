<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Delivery */

$this->title = Yii::t('app', 'Create Delivery');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Deliveries'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
if (Yii::$app->request->isAjax)
    echo \yii\widgets\Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]);
?>
<div class="delivery-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
