<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\OrderAddition */

$this->title = Yii::t('app', 'Create Order Addition');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Order Additions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-addition-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
