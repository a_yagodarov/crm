<?php

use yii\db\Migration;

class m160310_213247_create_task_table extends Migration
{
    public function up()
    {
        $this->createTable('task', [
            'id' => \yii\db\Schema::TYPE_PK,
            'task_type_id' => \yii\db\Schema::TYPE_INTEGER,
            'text' => \yii\db\Schema::TYPE_STRING.'(64000) NULL',
            'date_create' => \yii\db\Schema::TYPE_TIMESTAMP.' NULL',
            'date_end' => \yii\db\Schema::TYPE_TIMESTAMP.' NULL',
        ]);
    }

    public function down()
    {
        $this->dropTable('task');
    }
}
