<?php

use yii\db\Migration;

class m160310_110728_comment_length_64000 extends Migration
{
    public function up()
    {
        $this->alterColumn(\app\models\Order::tableName(), 'comment', \yii\db\Schema::TYPE_STRING.'(64000) NULL');
        $this->alterColumn(\app\models\Client::tableName(), 'comment', \yii\db\Schema::TYPE_STRING.'(64000) NULL');
    }

    public function down()
    {
        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
