<?php

use yii\db\Migration;

class m160313_200105_add_task_status extends Migration
{
    public function up()
    {
        $this->addColumn(\app\models\Task::tableName(), 'status', \yii\db\Schema::TYPE_INTEGER);
    }

    public function down()
    {
        $this->dropColumn(\app\models\Task::tableName(), 'status');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
