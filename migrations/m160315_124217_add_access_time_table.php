<?php

use yii\db\Migration;
use \yii\db\Schema;
class m160315_124217_add_access_time_table extends Migration
{
    public function up()
    {
        $this->addColumn(\app\models\Profile::tableName(), 'from', Schema::TYPE_INTEGER.' DEFAULT NULL');
        $this->addColumn(\app\models\Profile::tableName(), 'to', Schema::TYPE_INTEGER.' DEFAULT NULL');
        $this->addColumn(\app\models\Profile::tableName(), 'active', Schema::TYPE_INTEGER.' DEFAULT NULL');
    }

    public function down()
    {
        $this->dropColumn(\app\models\Profile::tableName(), 'from');
        $this->dropColumn(\app\models\Profile::tableName(), 'to');
        $this->dropColumn(\app\models\Profile::tableName(), 'active');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
