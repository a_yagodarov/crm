<?php

use yii\db\Migration;

class m160316_153728_add_apply_access extends Migration
{
    public function up()
    {
        $this->addColumn(\app\models\Profile::tableName(), 'apply', \yii\db\Schema::TYPE_INTEGER);
    }

    public function down()
    {
        $this->dropColumn(\app\models\Profile::tableName(), 'apply');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
