<?php

use yii\db\Migration;

class m160307_191158_add_weight_to_order_and_product extends Migration
{
    public function up()
    {
        $this->addColumn(\app\models\Order::tableName(), 'weight', \yii\db\Schema::TYPE_INTEGER.' DEFAULT 0');
        $this->addColumn(\app\models\Product::tableName(), 'weight', \yii\db\Schema::TYPE_INTEGER.' DEFAULT 0 AFTER price');
        $this->addColumn(\app\models\Product::tableName(), 'total_weight', \yii\db\Schema::TYPE_INTEGER.' DEFAULT 0 AFTER count');
    }

    public function down()
    {
        $this->dropColumn(\app\models\Order::tableName(), 'weight');
        $this->dropColumn(\app\models\Product::tableName(), 'weight');
        $this->dropColumn(\app\models\Product::tableName(), 'total_weight');
    }
}
