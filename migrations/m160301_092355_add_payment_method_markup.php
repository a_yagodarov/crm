<?php

use yii\db\Migration;

class m160301_092355_add_payment_method_markup extends Migration
{
    public function up()
    {
        $this->addColumn(\app\models\PaymentMethod::tableName(), 'markup', \yii\db\Schema::TYPE_INTEGER.' DEFAULT 0');
        $this->update(\app\models\PaymentMethod::tableName(), ['markup' => 5], ['id' => 1]);
    }

    public function down()
    {
        $this->dropColumn(\app\models\PaymentMethod::tableName(), 'markup');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
