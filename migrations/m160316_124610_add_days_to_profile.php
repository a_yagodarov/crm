<?php

use yii\db\Migration;

class m160316_124610_add_days_to_profile extends Migration
{
    public function up()
    {
        $this->addColumn(\app\models\Profile::tableName(), 'days', \yii\db\Schema::TYPE_STRING);
    }

    public function down()
    {
        $this->dropColumn(\app\models\Profile::tableName(), 'days');
    }
}
