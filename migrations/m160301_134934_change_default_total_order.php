<?php

use yii\db\Migration;

class m160301_134934_change_default_total_order extends Migration
{
    public function up()
    {
        $this->alterColumn(\app\models\Product::tableName(), 'total', \yii\db\Schema::TYPE_INTEGER.' DEFAULT 0 NOT NULL');
    }

    public function down()
    {
        $this->dropColumn(\app\models\Product::tableName(), 'total');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
