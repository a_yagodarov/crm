<?php

use yii\db\Migration;

class m160311_091038_add_foreign_key_task_client extends Migration
{
    public function up()
    {
        $this->addColumn(\app\models\Task::tableName(), 'client_id', \yii\db\Schema::TYPE_INTEGER.' NULL');
        $this
            ->addForeignKey(
                'task_client_id', \app\models\Task::tableName(), 'client_id', \app\models\Client::tableName(),
                'id', 'NO ACTION', 'NO ACTION');
        $this
            ->addForeignKey(
                'task_type_id', \app\models\Task::tableName(), 'task_type_id', \app\models\TaskType::tableName(),
                'id', 'NO ACTION', 'NO ACTION');
    }

    public function down()
    {
        $this->dropForeignKey('task_client_id', \app\models\Task::tableName());
        $this->dropForeignKey('task_type_id', \app\models\Task::tableName());
        $this->dropColumn(\app\models\Task::tableName(), 'client_id');
    }
}
