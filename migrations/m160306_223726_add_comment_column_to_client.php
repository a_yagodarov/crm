<?php

use yii\db\Migration;

class m160306_223726_add_comment_column_to_client extends Migration
{
    public function up()
    {
        $this->addColumn(\app\models\Client::tableName(), 'comment', \yii\db\Schema::TYPE_STRING.'(255) NULL');
    }

    public function down()
    {
        $this->dropColumn(\app\models\Client::tableName(), 'comment');
    }
}
