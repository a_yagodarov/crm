<?php

use yii\db\Migration;

class m160311_073955_create_task_types_table extends Migration
{
    public function up()
    {
        $this->createTable('task_type', [
            'id' => $this->primaryKey(),
            'name' => $this->string('128')->notNull()
        ]);
    }

    public function down()
    {
        $this->dropTable('task_type');
    }
}
