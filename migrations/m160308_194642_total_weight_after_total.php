<?php

use yii\db\Migration;

class m160308_194642_total_weight_after_total extends Migration
{
    public function up()
    {
        $this->alterColumn('product', 'total_weight', \yii\db\Schema::TYPE_INTEGER.' DEFAULT 0 AFTER count');
    }

    public function down()
    {
        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
