<?php

use yii\db\Migration;

class m160303_105507_add_order_number_send extends Migration
{
    public function up()
    {
        $this->addColumn('order', 'order_send_num', \yii\db\Schema::TYPE_INTEGER.' DEFAULT NULL');
    }

    public function down()
    {
        $this->dropColumn('order', 'order_send_num');
    }
}
