<?php

use yii\db\Migration;

class m160313_134829_foreign_key_client_address_cascade extends Migration
{
    public function up()
    {
        $this->dropForeignKey('task_client_id', \app\models\Task::tableName());
        $this
            ->addForeignKey(
                'task_client_id', \app\models\Task::tableName(), 'client_id', \app\models\Client::tableName(),
                'id', 'CASCADE', 'NO ACTION');
        //$this->dropForeignKey('fk_order_client_id', \app\models\Order::tableName());
        $this
            ->addForeignKey(
                'fk_order_client_id', \app\models\Order::tableName(), 'client_id', \app\models\Client::tableName(),
                'id', 'CASCADE', 'NO ACTION');
    }

    public function down()
    {
        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
