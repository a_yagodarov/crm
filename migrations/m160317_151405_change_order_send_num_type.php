<?php

use yii\db\Migration;

class m160317_151405_change_order_send_num_type extends Migration
{
    public function up()
    {
        $this->alterColumn(\app\models\Order::tableName(), 'order_send_num', \yii\db\Schema::TYPE_STRING.'(64)');
    }

    public function down()
    {
        $this->dropColumn(\app\models\Order::tableName(), 'order_send_num');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
