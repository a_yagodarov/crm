<?php

use yii\db\Migration;

class m160311_205639_insert_task_types extends Migration
{
    public function up()
    {
        $sql = "INSERT INTO `task_type` (`id`, `name`) VALUES
            (1, 'Встреча'),
            (2, 'Почта'),
            (3, 'Звонок'),
            (4, 'Другое');";
        $command = Yii::$app->db->createCommand($sql)->execute();
    }

    public function down()
    {
        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
