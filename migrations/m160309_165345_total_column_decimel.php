<?php

use yii\db\Migration;

class m160309_165345_total_column_decimel extends Migration
{
    public function up()
    {
        $this->alterColumn('order', 'total', \yii\db\Schema::TYPE_DECIMAL.'(10,2) DEFAULT 0');
    }

    public function down()
    {
        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
