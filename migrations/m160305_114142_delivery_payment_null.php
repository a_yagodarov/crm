<?php

use yii\db\Migration;

class m160305_114142_delivery_payment_null extends Migration
{
    public function up()
    {
        $this->alterColumn('order', 'delivery_method_id', \yii\db\Schema::TYPE_INTEGER.' DEFAULT NULL');
        $this->alterColumn('order', 'payment_method_id', \yii\db\Schema::TYPE_INTEGER.' DEFAULT NULL');
    }

    public function down()
    {
        $this->alterColumn('order', 'delivery_method_id', \yii\db\Schema::TYPE_INTEGER.' NOT NULL');
        $this->alterColumn('order', 'payment_method_id', \yii\db\Schema::TYPE_INTEGER.' NOT NULL');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
