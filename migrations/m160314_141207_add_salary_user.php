<?php

use yii\db\Migration;

class m160314_141207_add_salary_user extends Migration
{
    public function up()
    {
        $this->addColumn(\app\models\Profile::tableName(), 'salary', \yii\db\Schema::TYPE_INTEGER.' DEFAULT 0');
        $this->addColumn(\app\models\Profile::tableName(), 'phone', \yii\db\Schema::TYPE_STRING.' DEFAULT 0');
    }

    public function down()
    {
        $this->dropColumn(\app\models\Profile::tableName(), 'salary');
        $this->dropColumn(\app\models\Profile::tableName(), 'phone');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
