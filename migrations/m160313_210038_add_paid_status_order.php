<?php

use yii\db\Migration;

class m160313_210038_add_paid_status_order extends Migration
{
    public function up()
    {
        $this->addColumn(\app\models\Order::tableName(), 'paid_status', \yii\db\Schema::TYPE_INTEGER.' DEFAULT 1');
    }

    public function down()
    {
        $this->dropColumn(\app\models\Order::tableName(), 'paid_status');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
