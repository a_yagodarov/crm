<?php

use yii\db\Migration;

class m160305_110726_add_order_tracking_number extends Migration
{
    public function up()
    {
        $this->addColumn('order', 'tracking_number', \yii\db\Schema::TYPE_INTEGER.' DEFAULT NULL');
    }

    public function down()
    {
        $this->dropColumn('order', 'tracking_number');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
