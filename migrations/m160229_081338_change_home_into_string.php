<?php

use yii\db\Migration;

class m160229_081338_change_home_into_string extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%address}}', 'house', \yii\db\Schema::TYPE_STRING.'(25) NOT NULL');
    }

    public function down()
    {
        echo "m160229_081338_change_home_into_string cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
