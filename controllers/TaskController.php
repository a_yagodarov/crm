<?php

namespace app\controllers;

use Yii;
use app\models\Task;
use app\models\TaskSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * TaskController implements the CRUD actions for Task model.
 */
class TaskController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
//                        'actions'=>['@'],
                        'allow' => true,
                        'roles' => ['@'],
//                        'matchCallback' => function($rule, $action){
//                            return Yii::$app->user->identity->role_id == \app\models\User::ROLE_ADMIN;
//                        },
                    ],
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ]
                ],
            ]
        ];
    }

    /**
     * Lists all Task models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TaskSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Task model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Task model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Task();
        $model->client_id = Yii::$app->request->get('id');
        $model->date_create = date('Y-m-d H:i:s', time());
//	    $this->performAjaxValidation($model);
        if (Yii::$app->request->isAjax)
        {
            if ($model->load(Yii::$app->request->post()))
            {
	            $model->date_end = date('Y-m-d H:i:s', strtotime($model->date_end));
                if ($model->save())
                    return Yii::$app->runAction('/client/view', ['id' => $model->client_id]);
            }
            else
                return $this->renderAjax('create', [
                    'model' => $model,
                ]);
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->date_end = date('Y-m-d H:i:s', strtotime($model->date_end));
            if ($model->save())
                return $this->redirect(['/client/view', 'id' => $model->client_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Task model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

	    if (Yii::$app->request->isAjax)
	    {
		    if ($model->load(Yii::$app->request->post()))
		    {
			    $model->date_end = date('Y-m-d H:i:s', strtotime($model->date_end));
			    if ($model->save())
				    return Yii::$app->runAction('/client/view', ['id' => $model->client_id]);
		    }
		    else
			    return $this->renderAjax('update', [
				    'model' => $model,
			    ]);
	    }

        if ($model->load(Yii::$app->request->post())) {
	        $model->date_end = date('Y-m-d H:i:s', strtotime($model->date_end));
	        if ($model->save())
	        {
		        return $this->redirect(['/client/view', 'id' => $model->client_id]);
	        }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Task model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->delete();
        if (Yii::$app->request->isAjax)
            return Yii::$app->runAction('/client/view', ['id' => $model->client_id]);
        return $this->redirect('/client/view', ['id' => $model->client_id]);
    }

    /**
     * Finds the Task model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Task the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Task::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

	protected function performAjaxValidation($model)
	{
		if (Yii::$app->request->isAjax) {
			if ($model->load(Yii::$app->request->post())) {
				Yii::$app->response->format = Response::FORMAT_JSON;
				echo json_encode(ActiveForm::validate($model));
				Yii::$app->end();
			}
		}
	}
}
