<?php

namespace app\controllers;

use app\models\Address;
use app\models\Order;
use app\models\Profile;
use app\models\Task;
use app\models\TaskSearch;
use Yii;
use app\models\Client;
use app\models\ClientSearch;
use yii\bootstrap\ActiveForm;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\DataProviderInterface;
use yii\web\Response;

/**
 * ClientController implements the CRUD actions for Client model.
 */
class ClientController extends Controller
{
    public function beforeAction($action)
    {
        // your custom code here, if you want the code to run before action filters,
        // wich are triggered on the [[EVENT_BEFORE_ACTION]] event, e.g. PageCache or AccessControl

        if (!parent::beforeAction($action)) {
            return false;
        }

        if (!Profile::haveAccess())
            throw new ForbiddenHttpException('Отказано в дотступе по времени');
        // other custom code here

        return true; // or false to not run the action
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions'=>['index', 'view', 'create', 'update', 'render'],
                        'allow' => true,
                        'roles' => ['@']
                    ],
                    [
                        'actions'=>['index', 'view', 'create', 'update', 'render', 'delete'],
                        'allow' => true,
                        'matchCallback' => function($rule, $action){
                            return Yii::$app->user->identity->role_id == \app\models\User::ROLE_ADMIN;
                        },
                    ],
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ]
                ],
            ]
        ];
    }

    /**
     * Lists all Client models.
     * @return mixed
     */
    public function actionIndex()
    {
        $x = Yii::$app->request->queryParams;
	    $searchModel = new ClientSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if (Yii::$app->request->isAjax)
        {
            return $this->renderAjax('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Client model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $dataProviderAddress = Address::filterSearch(['client_id' => $id]);
        $dataProviderOrder = Order::filterSearch(['client_id' => $id]);
        $dataProviderTask = Task::filterSearch(['client_id' => $id]);
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('view', [
                'model' => $this->findModel($id),
                'dataProviderAddress' => $dataProviderAddress,
                'dataProviderOrder' => $dataProviderOrder,
                'dataProviderTask' => $dataProviderTask
            ]);
        }
        return $this->render('view', [
            'model' => $this->findModel($id),
            'dataProviderAddress' => $dataProviderAddress,
            'dataProviderOrder' => $dataProviderOrder,
            'dataProviderTask' => $dataProviderTask
        ]);
    }

    /**
     * Creates a new Client model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Client();
        if (Yii::$app->request->isAjax)
        {
            if ($model->load(Yii::$app->request->post())) {
                $model->user_id = Yii::$app->user->identity->getId();
                $model->created_at = time();
                $model->updated_at = time();

                if ($model->validate() && $model->save())
                {
                    return Yii::$app->runAction('client/view', ['id' => $model->id]);
                }
                {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return ActiveForm::validate($model);
//                    return $this->render('create', [
//                        'model' => $model,
//                    ]);
                }
            } else {
                return $this->renderAjax('create', [
                    'model' => $model,
                ]);
            }
        }
        if ($model->load(Yii::$app->request->post())) {
            $model->user_id = Yii::$app->user->identity->getId();
            $model->created_at = time();
            $model->updated_at = time();
            if ($model->validate() && $model->save())
            {
                if (Yii::$app->request->isAjax)
                {
                    return Yii::$app->runAction(['view', 'id' => $model->id]);
                }
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Client model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if (Yii::$app->request->isAjax)
        {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $model->updated_at = time();
                if ($model->save()) {
                    return Yii::$app->runAction('/client/view', ['id' => $model->id]);
                }
            }
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->updated_at = time();
            if ($model->save())
            {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionRender()
    {
        if (Yii::$app->request->isAjax)
        {
            if (!Yii::$app->user->getId() == 1)
                $searchModel = new ClientSearch(['user_id' => Yii::$app->user->getId()]);
            else
                $searchModel = new ClientSearch();
            $x = $_REQUEST;
            $dataProvider = $searchModel->search($_REQUEST);
            return $this->renderAjax('_client_list', [
                'dataProvider' => $dataProvider
            ]);
        }
    }

    /**
     * Deletes an existing Client model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        if (Yii::$app->request->isAjax)
            return Yii::$app->runAction('/client/index');
        return $this->render('index');
    }

    /**
     * Finds the Client model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Client the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Client::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
