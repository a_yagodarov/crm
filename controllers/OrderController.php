<?php

namespace app\controllers;

use app\models\OrderAddition;
use app\models\OrderSearch;
use app\models\Product;
use app\models\ProductSearch;
use app\models\ProductTemp;
use app\models\ProductTempSearch;
use app\models\Profile;
use Yii;
use app\models\Order;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
/**
 * OrderController implements the CRUD actions for Order model.
 */
class OrderController extends Controller
{
    public function beforeAction($action)
    {
        // your custom code here, if you want the code to run before action filters,
        // wich are triggered on the [[EVENT_BEFORE_ACTION]] event, e.g. PageCache or AccessControl

        if (!parent::beforeAction($action)) {
            return false;
        }

        if (!Profile::haveAccess())
            throw new ForbiddenHttpException('Отказано в дотступе по времени');
        // other custom code here

        return true; // or false to not run the action
    }
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
//                        'actions'=>['index'],
                        'allow' => true,
                        'roles' => ['@'],
//                        'matchCallback' => function($rule, $action){
//                            return Yii::$app->user->identity->role_id == \app\models\User::ROLE_ADMIN;
//                        },
                    ],
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ]
                ],
            ]
        ];
    }

    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if (Yii::$app->request->isAjax)
        {
            return $this->renderAjax('index', [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel
            ]);
        }
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel
        ]);
    }

    /**
     * Displays a single Order model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Order model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Order();
        $model->client_id = Yii::$app->request->get('client_id');
        $params['ProductTempSearch'] = ['user_id' => Yii::$app->user->getId()];
        //$model->products[] = Product::getProductArrayForForm();
        //AJAX
        if (Yii::$app->request->isAjax)
        {
            if ($model->load(Yii::$app->request->post())) {
                $model->date_create = time();
                $model->date_update = time();
                if ($model->save())
                {
                    Product::insertProducts($_POST['model']['products'], $model->id);
                    return Yii::$app->runAction('/client/view', ['id' => $model->client_id]);
                }
                else
                {
                    $model->productsForm = Product::getProductArrayForForm();
                    return $this->renderAjax('create', [
                        //'orderProducts' => $model->products,
                        'model' => $model,
                    ]);
                }
            }
            else {
                $model->productsForm = Product::getProductArrayForForm();
                $model->date_book = time();
                $model->order_status_id = 1;
                return $this->renderAjax('create', [
                    //'orderProducts' => $model->products,
                    'model' => $model,
                ]);
            }
        }
        // POST
        if ($model->load(Yii::$app->request->post())) {
            $model->date_create = time();
            $model->date_update = time();
            if ($model->save())
            {
                Product::insertProducts($_POST['model']['products'], $model->id);
                return $this->redirect(['/client/view', 'id' => $model->client_id]);
            }
            else{
                $model->productsForm = Product::getProductArrayForForm();
                return $this->render('create', [
                    //'orderProducts' => $model->products,
                    'model' => $model,
                ]);
            }
        } else {
            $model->productsForm = Product::getProductArrayForForm();
            $model->order_status_id = 1;
            $model->date_book = time();
            return $this->render('create', [
                //'orderProducts' => $model->products,
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Order model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $params['ProductSearch'] = ['order_id' => $model->id];
        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search($params);
        $model->productsForm = Product::getProductArrayForForm();
        $model->products = Product::getOrderProductsById($model->id);
        if (Yii::$app->request->isAjax)
        {
            if ($model->load(Yii::$app->request->post())) {
                $model->date_update = time();
                Product::insertProducts($_POST['model']['products'], $model->id);
                if ($model->validate() && $model->save())
                {
                    return Yii::$app->runAction('/client/view', ['id' => $model->client_id]);
                }
                else
                {
                    $e = $model->getErrors();
                }
            } else {
                return $this->renderAjax('update', [
                    'model' => $model,
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel
                ]);
            }
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->date_update = time();
            Product::insertProducts($_POST['model']['products'], $model->id);
            if ($model->validate() && $model->save())
            {
                return $this->redirect(['/client/view', 'id' => $model->client_id]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel
            ]);
        }
    }

    /**
     * Deletes an existing Order model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->delete();

        if (Yii::$app->request->isAjax)
            return Yii::$app->runAction('/client/view', ['id' => $model->client_id]);
        $urlBack = Url::toRoute(['/client/view', 'id' => $model->client_id]);
        return $this->redirect($urlBack);
    }

    /**
     * Finds the Order model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Order the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
