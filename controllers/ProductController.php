<?php

namespace app\controllers;

use app\models\Profile;
use Yii;
use app\models\Product;
use app\models\ProductSearch;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class ProductController extends Controller
{
//    public function behaviors()
//    {
//        return [
//            'verbs' => [
//                'class' => VerbFilter::className(),
//                'actions' => [
//                    'delete' => ['post'],
//                ],
//            ],
//        ];
//    }

    public function beforeAction($action)
    {
        // your custom code here, if you want the code to run before action filters,
        // wich are triggered on the [[EVENT_BEFORE_ACTION]] event, e.g. PageCache or AccessControl

        if (!parent::beforeAction($action)) {
            return false;
        }

        if (!Profile::haveAccess())
            throw new ForbiddenHttpException('Отказано в доступе по времени');
        // other custom code here

        return true; // or false to not run the action
    }

    /**
     * Lists all Product models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        if (Yii::$app->request->isAjax)
        {
            return $this->renderAjax('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Product model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (Yii::$app->request->isAjax)
        {
            return $this->renderAjax('view', [
                'model' => $this->findModel($id),
            ]);
        }
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Product model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Product();
        $model->order_id = Yii::$app->request->get('id');
        if (Yii::$app->request->isAjax)
        {
            if ($model->load(Yii::$app->request->post())) {
                if ($model->save())
                    return Yii::$app->runAction('order/update', ['id' => $model->order_id]);
            } else {
                $model->id = Yii::$app->request->get('id1');
                return $this->renderAjax('create', [
                    'model' => $model,
                ]);
            }
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (Yii::$app->request->isAjax)
                return Yii::$app->runAction('order/update', ['id' => $model->order_id]);
            return $this->redirect(['order/update', 'id' => $model->order_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Product model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if (Yii::$app->request->isAjax)
        {
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return Yii::$app->runAction('order/update', ['id' => $model->order_id]);
            } else {
                return $this->renderAjax('update', [
                    'model' => $model,
                ]);
            }
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['order/update', ['id' => $model->order_id]]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Product model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->delete();
        if (Yii::$app->request->isAjax)
        {
            $params['ProductSearch'] = ['order_id' => $model->order_id];
            $searchModel = new ProductSearch();
            $dataProvider = $searchModel->search($params);
            return $this->renderAjax('/order/_product_list', [
                'client_id' => $model->order_id,
                'dataProvider' => $dataProvider
            ]);
        }
        $params['ProductSearch'] = ['order_id' => $model->order_id];
        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search($params);
        return $this->render('/order/_product_list', [
            'client_id' => $model->order_id,
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
