<?php

namespace app\controllers;

use app\models\User;
use Yii;
use app\models\Address;
use app\models\AddressSearch;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\base\Widget;
/**
 * AddressController implements the CRUD actions for Address model.
 */
class AddressController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
//                        'actions'=>['index'],
                        'allow' => true,
                        'roles' => ['@'],
//                        'matchCallback' => function($rule, $action){
//                            return Yii::$app->user->identity->role_id == User::ROLE_ADMIN;
//                        },
                    ],
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ]
                ],
            ]
        ];
    }

    /**
     * Lists all Address models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AddressSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Address model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Address model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Address();
        $client_id = Yii::$app->request->get('id');
        if (Yii::$app->request->isAjax)
        {
            $data = Yii::$app->request->post();
            if ($model->load($data) && $model->validate())
            {
                $model->client_id = $client_id;
                //$returnUrl = Url::toRoute(['client/view']);
                if ($model->save())
                    return
                        Yii::$app->runAction('client/view', ['id' => $model->client_id]);
                        //'redirect'=> Url::toRoute('/client/view', ['id' => $model->client_id])

            }
            else
            {
                $x = $model->getErrors();
                return $this->renderAjax('create', [
                    'model' => $model,
                ]);
            }
        }
        if ($model->load(Yii::$app->request->post())) {
            $model->client_id = $client_id;
            if ($model->validate() && $model->save()) {
                $urlBack = Url::toRoute(['client/view', 'id' => $model->client_id]);
                return $this->redirect($urlBack);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Address model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (Yii::$app->request->isAjax)
            {
                return Yii::$app->runAction('client/view', ['id' => $model->client_id]);
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            if (Yii::$app->request->isAjax)
                return $this->renderAjax('update', [
                    'model' => $model,
                ]);
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Address model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->delete();

        if (Yii::$app->request->isAjax)
            return Yii::$app->runAction('/client/view', ['id' => $model->client_id]);
        $urlBack = Url::toRoute(['/client/view', 'id' => $model->client_id]);
        return $this->redirect($urlBack);
    }

    /**
     * Finds the Address model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Address the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Address::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
